init_db:
	env $$(cat .env) python -m src.storages.postgres.init

seed:
	env $$(cat .env) python -m src.storages.seeding.main

delete_db:
	env $$(cat .env) python -m src.storages.postgres.delete

init_ch:
	env $$(cat .env) python -m src.storages.clickhouse --init

delete_ch:
	env $$(cat .env) python -m src.storages.clickhouse --delete

etl:
	env $$(cat .env) python -m src.storages.etl

etl_clear:
	env $$(cat .env) python -m src.storages.etl.clear

app:
	python -m src.storages.application.app