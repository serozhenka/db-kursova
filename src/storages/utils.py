import time
from contextlib import contextmanager


@contextmanager
def measure_time(name: str):
    start = time.time()
    yield
    end = time.time()
    print(f"{name} succeded in {(end - start):.3f}s")
