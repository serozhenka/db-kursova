import numpy as np
import pandas as pd
from psycopg import Connection

from .schemas import Campaign


def get_campaigns(campaigns: pd.DataFrame) -> list[Campaign]:
    df = campaigns
    df.replace({"finished_at": {np.nan: None}}, inplace=True)

    return [
        Campaign(
            id=record["id"],
            name=f"Campaign {record['id']}",
            type=record["campaign_type"],
            channel=record["channel"],
            topic=record["topic"],
            is_ab_test=bool(record["ab_test"]),
            hour_sending_limit=record["hour_limit"],
            subject_length=record["subject_length"],
            subject_with_personalization=record["subject_with_personalization"],
            subject_with_deadline=record["subject_with_deadline"],
            subject_with_emoji=record["subject_with_emoji"],
            subject_with_bonuses=record["subject_with_bonuses"],
            subject_with_discount=record["subject_with_discount"],
            subject_with_saleout=record["subject_with_saleout"],
            priority=record["position"],
            started_at=record["started_at"],
            finished_at=record["finished_at"],
        )
        for record in df.to_dict(orient="records")
    ]


def insert_campaigns(conn: Connection, campaigns: list[Campaign]) -> None:
    with conn.cursor() as cursor:
        cursor.executemany(
            """
            INSERT INTO campaigns(
                id,
                name,
                type_id,
                channel_id,
                topic_id,
                is_ab_test,
                hour_sending_limit,
                subject_length,
                subject_with_personalization,
                subject_with_deadline,
                subject_with_emoji,
                subject_with_bonuses,
                subject_with_discount,
                subject_with_saleout,
                priority,
                started_at,
                finished_at
            )
            VALUES (
                %(id)s,
                %(name)s,
                (
                    SELECT id
                    FROM message_types
                    WHERE name = %(type)s
                ),
                (
                    SELECT id
                    FROM delivery_channels
                    WHERE name = %(channel)s
                ),
                (
                    SELECT id
                    FROM topics
                    WHERE name = %(topic)s
                ),
                %(is_ab_test)s,
                %(hour_sending_limit)s,
                %(subject_length)s,
                %(subject_with_personalization)s,
                %(subject_with_deadline)s,
                %(subject_with_emoji)s,
                %(subject_with_bonuses)s,
                %(subject_with_discount)s,
                %(subject_with_saleout)s,
                %(priority)s,
                %(started_at)s,
                %(finished_at)s
            )
            ON CONFLICT DO NOTHING
            """,
            [
                {
                    "id": campaign.id,
                    "name": campaign.name,
                    "type": campaign.type,
                    "channel": campaign.channel,
                    "topic": campaign.topic,
                    "is_ab_test": campaign.is_ab_test,
                    "hour_sending_limit": campaign.hour_sending_limit,
                    "subject_length": campaign.subject_length,
                    "subject_with_personalization": campaign.subject_with_personalization,
                    "subject_with_deadline": campaign.subject_with_deadline,
                    "subject_with_emoji": campaign.subject_with_emoji,
                    "subject_with_bonuses": campaign.subject_with_bonuses,
                    "subject_with_discount": campaign.subject_with_discount,
                    "subject_with_saleout": campaign.subject_with_saleout,
                    "priority": campaign.priority,
                    "started_at": campaign.started_at,
                    "finished_at": campaign.finished_at,
                }
                for campaign in campaigns
            ],
        )

        conn.commit()
