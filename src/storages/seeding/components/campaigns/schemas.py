from datetime import datetime

from pydantic import BaseModel


class Campaign(BaseModel):
    id: int
    name: str
    type: str
    channel: str
    topic: str
    is_ab_test: bool
    hour_sending_limit: int
    subject_length: int
    subject_with_personalization: bool
    subject_with_deadline: bool
    subject_with_emoji: bool
    subject_with_bonuses: bool
    subject_with_discount: bool
    subject_with_saleout: bool
    priority: int | None
    started_at: datetime
    finished_at: datetime | None
