from collections import defaultdict
from decimal import Decimal

import pandas as pd
from mimesis import Generic
from pluralizer import Pluralizer
from psycopg import Connection

from .schemas import Product


def get_products(purchases: pd.DataFrame) -> list[Product]:
    df = purchases.drop_duplicates("product_id")
    faker = Generic()
    pluralizer = Pluralizer()

    results: list[Product] = []
    product_counter: dict[str, int] = defaultdict(int)

    for record in df.to_dict(orient="records"):
        category_code = record["category_code"]
        name = (
            record["brand"].capitalize()
            + " "
            + pluralizer.singular(category_code.split(".")[-1]).capitalize()
        )
        product_counter[name] += 1

        results.append(
            Product(
                id=record["product_id"],
                name=f"{name} {product_counter[name]}",
                description=name,
                image_url=faker.internet.stock_image_url(),
                category_name=category_code,
                brand_name=record["brand"],
                price=Decimal(record["price"]),
            )
        )

    return results


def insert_products(conn: Connection, products: list[Product]) -> None:
    with conn.cursor() as cursor:
        cursor.executemany(
            """
            INSERT INTO products(
                id, 
                name,
                description,
                image_url,
                category_id,
                brand_id,
                price
            )
            VALUES (
                %(id)s,
                %(name)s,
                %(description)s,
                %(image_url)s,
                (
                    WITH RECURSIVE cte AS (
                        SELECT 
                            id,
                            parent_id,
                            name,
                            name::text as full_name,
                            parent_id as initial_parent_id,
                            id as initial_id
                        FROM categories
                            
                        UNION
                        
                        SELECT 
                            c.id,
                            c.parent_id,
                            c.name,
                            (c.name || '.' || cte.full_name)::text,
                            cte.initial_parent_id as initial_parent_id,
                            cte.initial_id
                        FROM categories c
                        JOIN cte on c.id = cte.parent_id
                    )
                    SELECT cte.initial_id
                    FROM cte
                    WHERE 
                        cte.parent_id IS NULL 
                        AND cte.full_name = %(category_name)s
                ),
                (
                    SELECT id
                    FROM brands
                    WHERE name = %(brand_name)s
                ),
                %(price)s
            )
            ON CONFLICT DO NOTHING
            """,
            [
                {
                    "id": product.id,
                    "name": product.name,
                    "description": product.description,
                    "image_url": product.image_url,
                    "category_name": product.category_name,
                    "brand_name": product.brand_name,
                    "price": product.price,
                }
                for product in products
            ],
        )
        cursor.execute(
            "SELECT setval('products_id_seq', %(max_id)s, true)",
            params={"max_id": max(p.id for p in products)},
        )
        conn.commit()
