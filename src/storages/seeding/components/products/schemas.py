from decimal import Decimal

from pydantic import BaseModel


class Product(BaseModel):
    id: int
    name: str
    description: str
    image_url: str
    category_name: str | None
    brand_name: str
    price: Decimal
