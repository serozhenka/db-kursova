from mimesis import Locale

LOCALES_TO_COUNTRY_CODES: dict[Locale, str] = {
    Locale.EN_GB: "gb",
    Locale.ES: "es",
    Locale.FR: "fr",
    Locale.DE: "de",
    Locale.IT: "it",
    Locale.PL: "pl",
    Locale.UK: "ua",
}
