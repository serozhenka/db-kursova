from datetime import datetime
from decimal import Decimal

from pydantic import BaseModel


class Address(BaseModel):
    country: str
    city: str
    street: str
    appartment: int


class OrderLine(BaseModel):
    order_id: int
    product_id: int
    price: Decimal
    quantity: int
    created_at: datetime


class Order(BaseModel):
    id: int
    customer_id: int
    address: Address
    created_at: datetime


class CountryConfig(BaseModel):
    cities: list[str]
    streets: list[str]
