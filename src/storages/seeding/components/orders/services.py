import random
from decimal import Decimal

import pandas as pd
from mimesis import Generic
from psycopg import Connection

from ..common.locales import LOCALES_TO_COUNTRY_CODES
from .constants import MIN_ORDER_DATE
from .schemas import Address, CountryConfig, Order, OrderLine


def get_orders(purchases: pd.DataFrame) -> list[Order]:
    df = purchases.drop_duplicates("order_id")

    addresses: dict[str, CountryConfig] = {}

    for locale in LOCALES_TO_COUNTRY_CODES.keys():
        faker = Generic(locale=locale)
        country = LOCALES_TO_COUNTRY_CODES[locale]
        cities = [faker.address.city() for _ in range(20)]
        streets = [faker.address.street_name() for _ in range(20)]
        addresses[country] = CountryConfig(cities=cities, streets=streets)

    results: list[Order] = []
    countries = list(LOCALES_TO_COUNTRY_CODES.values())

    for record in df.to_dict(orient="records"):
        country = random.choice(countries)
        city = random.choice(addresses[country].cities)
        street = random.choice(addresses[country].streets)
        appartment = int(random.random() * 100 + 1)

        if record["event_time"] < MIN_ORDER_DATE:
            continue

        order = Order(
            id=record["order_id"],
            customer_id=record["user_id"],
            address=Address(
                country=country,
                city=city,
                street=street,
                appartment=appartment,
            ),
            created_at=record["event_time"],
        )
        results.append(order)

    return results


def get_order_lines(purchases: pd.DataFrame) -> list[OrderLine]:
    order_lines: dict[tuple[int, int], OrderLine] = {}

    for record in purchases.to_dict(orient="records"):
        order_line = OrderLine(
            order_id=record["order_id"],
            product_id=record["product_id"],
            price=Decimal(record["price"]),
            quantity=1,
            created_at=record["event_time"],
        )

        if record["event_time"] < MIN_ORDER_DATE:
            continue

        key = (order_line.order_id, order_line.product_id)
        if existing := order_lines.get(key):
            existing.quantity += order_line.quantity
            existing.price += order_line.price
        else:
            order_lines[key] = order_line

    return list(order_lines.values())


def insert_orders(conn: Connection, orders: list[Order]) -> None:
    with conn.cursor() as cursor:
        with cursor.copy(
            """
            COPY addresses(id, country, city, street, appartment) 
            FROM STDIN
            """
        ) as copy:
            for idx, order in enumerate(orders):
                copy.write_row(
                    (
                        idx + 1,
                        order.address.country,
                        order.address.city,
                        order.address.street[:64],
                        order.address.appartment,
                    )
                )

        cursor.execute(
            "SELECT setval('addresses_id_seq', %(max_id)s, true)",
            params={"max_id": len(orders)},
        )

        with cursor.copy(
            """
            COPY orders(id, status, customer_id, address_id, created_at, updated_at)
            FROM STDIN
            """
        ) as copy:
            for idx, order in enumerate(orders):
                copy.write_row(
                    (
                        order.id,
                        "CLOSED",
                        int(order.customer_id),
                        idx + 1,
                        order.created_at,
                        order.created_at,
                    )
                )

        cursor.execute(
            "SELECT setval('orders_id_seq', %(max_id)s, true)",
            params={"max_id": max(o.id for o in orders)},
        )

        conn.commit()


def insert_order_lines(conn: Connection, order_lines: list[OrderLine]) -> None:
    with conn.cursor() as cursor:
        with cursor.copy(
            """
            COPY order_lines(
                order_id,
                product_id,
                price,
                quantity,
                created_at,
                updated_at
            ) FROM STDIN
            """
        ) as copy:
            for ol in order_lines:
                copy.write_row(
                    (
                        ol.order_id,
                        ol.product_id,
                        ol.price,
                        ol.quantity,
                        ol.created_at,
                        ol.created_at,
                    )
                )

        conn.commit()
