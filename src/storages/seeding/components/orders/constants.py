from datetime import datetime, timezone

MIN_ORDER_DATE = datetime(year=2020, month=1, day=1, tzinfo=timezone.utc)
