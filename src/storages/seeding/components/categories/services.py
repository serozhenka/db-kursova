import itertools

import pandas as pd
from psycopg import Connection

from .schemas import Category


def get_categories(purchases: pd.DataFrame) -> list[Category]:
    df = purchases[["category_id", "category_code"]]
    df = df[~df["category_code"].isna() & ~df["category_id"].isna()]
    df = df.drop_duplicates("category_code")

    results: set[Category] = set()

    for category_full_name in df["category_code"]:
        categories = category_full_name.split(".")
        parent_category = None

        for category_str in categories:
            category = Category(name=category_str, parent=parent_category)
            results.add(category)
            parent_category = category

    return list(results)


def insert_categories(conn: Connection, categories: list[Category]) -> None:
    categories = sorted(categories, key=lambda c: c.distance)

    with conn.cursor() as cursor:
        for _, category_group in itertools.groupby(
            categories,
            key=lambda c: c.distance,
        ):
            cursor.executemany(
                """
                INSERT INTO categories(name, parent_id)
                VALUES (
                    %(name)s,
                    (
                        WITH RECURSIVE cte AS (
                            SELECT 
                                id, 
                                parent_id, 
                                name,
                                name::text AS full_name
                            FROM categories
                            WHERE parent_id is NULL

                            UNION ALL

                            SELECT 
                                c.id,
                                c.parent_id,
                                c.name,
                                (cte.full_name || '.' || c.name)::text
                            FROM categories c
                            JOIN cte on c.parent_id = cte.id
                        )
                        SELECT id
                        FROM cte
                        WHERE full_name = %(parent_name)s
                    )
                )
                ON CONFLICT DO NOTHING
                """,
                [
                    {
                        "name": c.name,
                        "parent_name": c.parent.full_name if c.parent else None,
                    }
                    for c in category_group
                ],
            )
            conn.commit()
