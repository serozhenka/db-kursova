from pydantic import BaseModel


class Category(BaseModel):
    name: str
    parent: "Category | None"

    @property
    def full_name(self) -> str:
        categories = [self.name]
        current = self
        while current.parent is not None:
            categories.append(current.parent.name)
            current = current.parent

        return ".".join(categories[::-1])

    @property
    def distance(self):
        return self.full_name.count(".") + 1

    def __eq__(self, other: "Category") -> bool:
        if other is None:
            return False

        return self.name == other.name and self.parent == other.parent

    def __hash__(self) -> int:
        return hash(self.full_name)
