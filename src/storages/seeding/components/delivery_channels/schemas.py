from pydantic import BaseModel


class DeliveryChannel(BaseModel, frozen=True):
    name: str
