import pandas as pd
from psycopg import Connection

from .schemas import DeliveryChannel


def get_delivery_channels(campaigns: pd.DataFrame) -> set[DeliveryChannel]:
    df = campaigns.drop_duplicates("channel")
    return set(DeliveryChannel(name=name) for name in df["channel"])


def insert_delivery_channels(
    conn: Connection,
    delivery_channels: set[DeliveryChannel],
) -> None:
    with conn.cursor() as cursor:
        with cursor.copy("COPY delivery_channels (name) FROM STDIN") as copy:
            for delivery_channel in delivery_channels:
                copy.write_row((delivery_channel.name,))

        conn.commit()
