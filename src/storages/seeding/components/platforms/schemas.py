from pydantic import BaseModel


class Platform(BaseModel, frozen=True):
    name: str
