import pandas as pd
from psycopg import Connection

from .schemas import Platform


def get_platforms(messages: pd.DataFrame) -> set[Platform]:
    df = messages[["platform"]]
    df = df[~df["platform"].isnull() & ~df["platform"].isna()]
    df = df.drop_duplicates("platform")
    return {Platform(name=name) for name in df["platform"]}


def insert_platforms(conn: Connection, platforms: set[Platform]) -> None:
    with conn.cursor() as cursor:
        with cursor.copy("COPY platforms (name) FROM STDIN") as copy:
            for platform in platforms:
                copy.write_row((platform.name,))

        conn.commit()
