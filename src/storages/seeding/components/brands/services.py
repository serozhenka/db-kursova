import pandas as pd
from mimesis import Generic
from psycopg import Connection

from .schemas import Brand


def get_brands(purchases: pd.DataFrame) -> list[Brand]:
    df = purchases[["brand"]]
    df = df[~df["brand"].isna()]
    df = df[df["brand"].str.isalpha()]
    df = df.drop_duplicates("brand")

    faker = Generic()

    return [
        Brand(name=brand, image_url=faker.internet.stock_image_url())
        for brand in df["brand"]
    ]


def insert_brands(conn: Connection, brands: list[Brand]) -> None:
    with conn.cursor() as cursor:
        with cursor.copy("COPY brands(name, image_url) FROM STDIN") as copy:
            for brand in brands:
                copy.write_row((brand.name, brand.image_url))

        conn.commit()
