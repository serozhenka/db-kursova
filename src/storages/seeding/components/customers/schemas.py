from datetime import date

from pydantic import BaseModel


class Customer(BaseModel):
    id: int
    first_name: str
    last_name: str
    email: str
    phone_number: str
    birthdate: date
    gender: str
    country_code: str
