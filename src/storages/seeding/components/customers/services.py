import random

import pandas as pd
from mimesis import Gender, Person
from mimesis.locales import Locale
from psycopg import Connection

from ..common.locales import LOCALES_TO_COUNTRY_CODES
from .schemas import Customer


def get_customers(purchases: pd.DataFrame) -> list[Customer]:
    df = purchases[["user_id"]]
    df = df[pd.to_numeric(df["user_id"], errors="coerce").notnull()]
    df = df.drop_duplicates("user_id")

    genders: list[Gender] = [Gender.MALE, Gender.FEMALE]
    locales: list[Locale] = list(LOCALES_TO_COUNTRY_CODES.keys())
    people: dict[Locale, Person] = {
        locale: Person(locale=locale) for locale in locales
    }

    results: list[Customer] = []

    for customer_id in df["user_id"]:
        gender = random.choice(genders)
        locale = random.choice(locales)
        person = people[locale]
        full_name = person.full_name(gender=gender)
        first_name, last_name = full_name.split(" ", 1)

        results.append(
            Customer(
                id=customer_id,
                first_name=first_name,
                last_name=last_name,
                email=person.email(unique=True),
                phone_number=person.phone_number(),
                birthdate=person.birthdate(min_year=1980, max_year=2005),
                gender=gender.value.upper(),
                country_code=LOCALES_TO_COUNTRY_CODES[locale],
            )
        )

    return results


def insert_customers(conn: Connection, customers: list[Customer]) -> None:
    with conn.cursor() as cursor:
        cursor.executemany(
            """
            INSERT INTO customers(
                id,
                first_name,
                last_name,
                email,
                phone_number,
                birthdate,
                gender,
                country_code
            )
            VALUES (
                %(id)s,
                %(first_name)s,
                %(last_name)s,
                %(email)s,
                %(phone_number)s,
                %(birthdate)s,
                %(gender)s,
                %(country_code)s
            )
            ON CONFLICT DO NOTHING
            """,
            [
                {
                    "id": customer.id,
                    "first_name": customer.first_name,
                    "last_name": customer.last_name,
                    "email": customer.email,
                    "phone_number": customer.phone_number,
                    "birthdate": customer.birthdate,
                    "gender": customer.gender,
                    "country_code": customer.country_code,
                }
                for customer in customers
            ],
        )

        cursor.execute(
            "SELECT setval('customers_id_seq', %(max_id)s, true)",
            params={"max_id": int(max(p.id for p in customers))},
        )
        conn.commit()
