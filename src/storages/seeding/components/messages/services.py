import random
from datetime import datetime, timedelta, timezone
from functools import partial

import numpy as np
import pandas as pd
from psycopg import Connection

from ..customers.schemas import Customer
from ..orders.schemas import Order
from .schemas import Message


def fit_datetime(
    dt: datetime,
    start: datetime,
    end: datetime,
    reference_start: datetime,
    reference_end: datetime,
) -> datetime:
    position = (dt - start).total_seconds() / (end - start).total_seconds()
    reference_diff = (reference_end - reference_start).total_seconds()
    return reference_start + timedelta(seconds=int(reference_diff * position))


def get_messages(messages: pd.DataFrame) -> list[Message]:
    df = messages
    df = df.drop_duplicates(["message_id"])
    df = df.replace({np.nan: None})

    return [
        Message(
            id=record["message_id"],
            campaign_id=record["campaign_id"],
            platform=record["platform"],
            opened_at=record["opened_first_time_at"],
            clicked_at=record["clicked_first_time_at"],
            unsubscribed_at=record["unsubscribed_at"],
            purchased_at=record["purchased_at"],
            sent_at=record["sent_at"],
        )
        for record in df.to_dict(orient="records")
    ]


def insert_messages(
    conn: Connection,
    messages: list[Message],
    customers: list[Customer],
    orders: list[Order],
) -> None:
    min_message_dt = datetime(year=2021, month=1, day=1, tzinfo=timezone.utc)
    max_message_dt = datetime(year=2022, month=1, day=1, tzinfo=timezone.utc)
    min_order_dt = min(o.created_at for o in orders)
    max_order_dt = max(o.created_at for o in orders)

    fit_dt = partial(
        fit_datetime,
        start=min_message_dt,
        end=max_message_dt,
        reference_start=min_order_dt,
        reference_end=max_order_dt,
    )

    orders = sorted(orders, key=lambda o: o.created_at)

    insert_values = []
    for message in messages:
        if message.clicked_at is not None:
            message.clicked_at = fit_dt(message.clicked_at)

        if message.opened_at is not None:
            message.opened_at = fit_dt(message.opened_at)
            if message.clicked_at is None:
                message.opened_at = message.clicked_at

        if message.unsubscribed_at is not None:
            message.unsubscribed_at = fit_dt(message.unsubscribed_at)

        if message.sent_at is not None:
            message.sent_at = fit_dt(message.sent_at)

        if message.purchased_at is not None:
            message.purchased_at = fit_dt(message.purchased_at)
            order = next(
                o for o in orders if o.created_at >= message.purchased_at
            )
            order_id = order.id
            customer_id = order.customer_id
        else:
            customer = random.choice(customers)
            order_id = None
            customer_id = customer.id

        insert_value = {
            "id": message.id,
            "campaign_id": message.campaign_id,
            "customer_id": customer_id,
            "order_id": order_id,
            "platform": message.platform,
            "opened_at": message.opened_at,
            "clicked_at": message.clicked_at,
            "unsubscribed_at": message.unsubscribed_at,
            "sent_at": message.sent_at,
        }
        insert_values.append(insert_value)

    with conn.cursor() as cursor:
        cursor.execute("SELECT id, name FROM platforms")
        platforms_name_to_id = {r[1]: r[0] for r in cursor.fetchall()}

        with cursor.copy(
            """
            COPY messages(
                id,
                campaign_id,
                customer_id,
                platform_id,
                opened_at,
                clicked_at,
                unsubscribed_at,
                order_id,
                sent_at,
                updated_at,
                created_at
            )
            FROM STDIN
            """
        ) as copy:
            for row in insert_values:
                copy.write_row(
                    (
                        row["id"],
                        row["campaign_id"],
                        int(row["customer_id"]),
                        platforms_name_to_id.get(row["platform"]),
                        row["opened_at"],
                        row["clicked_at"],
                        row["unsubscribed_at"],
                        row["order_id"],
                        row["sent_at"],
                        row["sent_at"],
                        row["sent_at"],
                    )
                )

        # cursor.executemany(
        #     """
        #     INSERT INTO messages(
        #         id,
        #         campaign_id,
        #         customer_id,
        #         platform_id,
        #         opened_at,
        #         clicked_at,
        #         unsubscribed_at,
        #         order_id,
        #         sent_at,
        #         updated_at,
        #         created_at
        #     )
        #     VALUES (
        #         %(id)s,
        #         %(campaign_id)s,
        #         %(customer_id)s,
        #         (
        #             SELECT id
        #             FROM platforms
        #             WHERE name = %(platform)s
        #         ),
        #         %(opened_at)s,
        #         %(clicked_at)s,
        #         %(unsubscribed_at)s,
        #         %(order_id)s,
        #         %(sent_at)s,
        #         %(sent_at)s,
        #         %(sent_at)s
        #     )
        #     ON CONFLICT DO NOTHING
        #     """,
        #     insert_values,
        # )

        conn.commit()
