from datetime import datetime

from pydantic import BaseModel


class Message(BaseModel):
    id: str
    campaign_id: int
    platform: str | None
    opened_at: datetime | None
    clicked_at: datetime | None
    unsubscribed_at: datetime | None
    purchased_at: datetime | None
    sent_at: datetime
