from psycopg import Connection


def insert_countries(
    conn: Connection,
    countries: list[tuple[str, str]],
) -> None:
    with conn.cursor() as cursor:
        with cursor.copy("COPY countries(alpha2, name) FROM STDIN") as copy:
            for alpha2, name in countries:
                copy.write_row((alpha2, name))

        conn.commit()
