import pandas as pd
from psycopg import Connection

from .schemas import MessageType


def get_message_types(campaigns: pd.DataFrame) -> set[MessageType]:
    df = campaigns.drop_duplicates("campaign_type")
    return set(MessageType(name=name) for name in df["campaign_type"])


def insert_message_types(
    conn: Connection,
    message_types: set[MessageType],
) -> None:
    with conn.cursor() as cursor:
        with cursor.copy("COPY message_types(name) FROM STDIN") as copy:
            for message_type in message_types:
                copy.write_row((message_type.name,))

        conn.commit()
