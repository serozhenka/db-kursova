from pydantic import BaseModel


class MessageType(BaseModel, frozen=True):
    name: str
