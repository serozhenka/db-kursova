from pydantic import BaseModel


class Topic(BaseModel, frozen=True):
    name: str
