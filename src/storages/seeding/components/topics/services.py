import pandas as pd
from psycopg import Connection
from pydantic import BaseModel


class Topic(BaseModel, frozen=True):
    name: str


def get_topics(campaigns: pd.DataFrame) -> set[Topic]:
    df = campaigns[["topic"]]
    df = df.drop_duplicates("topic")
    return {Topic(name=name) for name in df["topic"]}


def insert_topics(conn: Connection, topics: set[Topic]) -> None:
    with conn.cursor() as cursor:
        with cursor.copy("COPY topics(name) from STDIN") as copy:
            for topic in topics:
                copy.write_row((topic.name,))

        conn.commit()
