import os

import pandas as pd

purchases = pd.read_csv(
    os.path.join(
        os.path.dirname(__file__),
        "../data/purchases-cleared.csv",
    ),
    usecols=[
        "user_id",
        "order_id",
        "product_id",
        "user_id",
        "event_time",
        "category_id",
        "category_code",
        "brand",
        "price",
    ],
    date_format="%Y-%m-%d %H:%M:%S %Z",
    parse_dates=["event_time"],
)
