import os

import pandas as pd

campaigns = pd.read_csv(
    os.path.join(
        os.path.dirname(__file__),
        "../data/campaigns-cleared.csv",
    ),
    date_format="%Y-%m-%d %H:%M:%S",
    parse_dates=["started_at", "finished_at"],
)
