import os

import pandas as pd

messages = pd.read_csv(
    os.path.join(os.path.dirname(__file__), "../data/messages_cleared.csv"),
    usecols=[
        "message_id",
        "campaign_id",
        "platform",
        "opened_first_time_at",
        "clicked_first_time_at",
        "unsubscribed_at",
        "purchased_at",
        "sent_at",
    ],
    parse_dates=[
        "opened_first_time_at",
        "clicked_first_time_at",
        "unsubscribed_at",
        "purchased_at",
        "sent_at",
    ],
    date_format="%Y-%m-%d %H:%M:%S",
    nrows=5_000_000,
)
