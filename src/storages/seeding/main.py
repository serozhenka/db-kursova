import time

from psycopg import Connection

from storages.settings import settings
from storages.utils import measure_time

from .components.brands import get_brands, insert_brands
from .components.campaigns import get_campaigns, insert_campaigns
from .components.categories import get_categories, insert_categories
from .components.countries import COUNTRIES, insert_countries
from .components.customers import get_customers, insert_customers
from .components.delivery_channels import (
    get_delivery_channels,
    insert_delivery_channels,
)
from .components.message_types import (
    get_message_types,
    insert_message_types,
)
from .components.messages import get_messages, insert_messages
from .components.orders import (
    get_order_lines,
    get_orders,
    insert_order_lines,
    insert_orders,
)
from .components.platforms import get_platforms, insert_platforms
from .components.products import get_products, insert_products
from .components.topics import get_topics, insert_topics


def main():
    start = time.time()
    from .datasets.campaigns import campaigns as campaigns_dataset

    end = time.time()
    yield f"Imported campaigns dataset in {(end - start):.3f}s"

    start = time.time()
    from .datasets.messages import messages as messages_dataset

    end = time.time()
    yield f"Imported messages dataset in {(end - start):.3f}s"

    start = time.time()
    from .datasets.purchases import purchases as purchases_dataset

    end = time.time()
    yield f"Imported messages dataset in {(end - start):.3f}s"

    conn = Connection.connect(settings.db_url)

    categories = get_categories(purchases=purchases_dataset)
    yield f"Got {len(categories)} categories"
    insert_categories(conn=conn, categories=categories)
    yield f"Inserted {len(categories)} categories"

    brands = get_brands(purchases=purchases_dataset)
    yield f"Got {len(brands)} brands"
    insert_brands(conn=conn, brands=brands)
    yield f"Inserted {len(brands)} brands"

    products = get_products(purchases=purchases_dataset)
    yield f"Got {len(products)} products"
    insert_products(conn=conn, products=products)
    yield f"Inserted {len(products)} products"

    insert_countries(conn=conn, countries=COUNTRIES)
    yield f"Inserted {len(COUNTRIES)} countries"

    customers = get_customers(purchases=purchases_dataset)
    yield f"Got {len(customers)} customers"
    insert_customers(conn=conn, customers=customers)
    yield f"Inserted {len(customers)} customers"

    orders = get_orders(purchases=purchases_dataset)
    yield f"Got {len(orders)} orders"
    insert_orders(conn=conn, orders=orders)
    yield f"Inserted {len(orders)} orders"

    order_lines = get_order_lines(purchases=purchases_dataset)
    yield f"Got {len(order_lines)} order lines"
    insert_order_lines(conn=conn, order_lines=order_lines)
    yield f"Inserted {len(order_lines)} order lines"

    message_types = get_message_types(campaigns=campaigns_dataset)
    yield f"Got {len(message_types)} message types"
    insert_message_types(conn=conn, message_types=message_types)
    yield f"Inserted {len(message_types)} message types"

    delivery_channels = get_delivery_channels(campaigns=campaigns_dataset)
    yield f"Got {len(delivery_channels)} delivery channels"
    insert_delivery_channels(conn=conn, delivery_channels=delivery_channels)
    yield f"Inserted {len(delivery_channels)} delivery channels"

    platforms = get_platforms(messages=messages_dataset)
    yield f"Got {len(platforms)} platforms"
    insert_platforms(conn=conn, platforms=platforms)
    yield f"Inserted {len(platforms)} platforms"

    topics = get_topics(campaigns=campaigns_dataset)
    yield f"Got {len(topics)} topics"
    insert_topics(conn=conn, topics=topics)
    yield f"Inserted {len(topics)} topics"

    campaigns = get_campaigns(campaigns=campaigns_dataset)
    yield f"Got {len(campaigns)} campaigns"
    insert_campaigns(conn=conn, campaigns=campaigns)
    yield f"Inserted {len(campaigns)} campaigns"

    messages = get_messages(messages=messages_dataset)
    yield f"Got {len(messages)} messages"
    with measure_time("Insert messages"):
        insert_messages(
            conn=conn,
            messages=messages,
            orders=orders,
            customers=customers,
        )
        yield f"Inserted {len(messages)} messages"


if __name__ == "__main__":
    for message in main():
        print(message)
