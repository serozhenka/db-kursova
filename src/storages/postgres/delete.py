import os
from typing import LiteralString, cast

from psycopg import Connection

from storages.settings import settings


def main():
    conn = Connection.connect(settings.db_url)
    with (
        conn.cursor() as cursor,
        open(
            os.path.join(os.path.dirname(__file__), "scripts/delete.sql")
        ) as file,
    ):
        sql = cast(LiteralString, file.read())
        cursor.execute(sql)
        conn.commit()


if __name__ == "__main__":
    main()
