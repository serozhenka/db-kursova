CREATE OR REPLACE FUNCTION set_updated_at()
RETURNS TRIGGER AS $$
BEGIN
    NEW.updated_at = NOW();
    RETURN NEW;
END;
$$ LANGUAGE plpgsql;


CREATE TYPE order_status AS ENUM (
  'CREATED',
  'PAYED',
  'READY_FOR_DELIVERY',
  'DELIVERING',
  'DELIVERED',
  'CLOSED'
);

CREATE TYPE gender AS ENUM (
  'MALE',
  'FEMALE'
);

CREATE TABLE categories (
  "id" serial PRIMARY KEY NOT NULL,
  "name" varchar(64) NOT NULL,
  "parent_id" integer REFERENCES categories(id) ON DELETE CASCADE,
  "updated_at" timestamptz NOT NULL DEFAULT now(),
  "created_at" timestamptz NOT NULL DEFAULT now()
);

ALTER TABLE categories 
ADD CONSTRAINT categories_name_parent_id_uq
UNIQUE NULLS NOT DISTINCT (name, parent_id);

CREATE TRIGGER categories_updated_at_trigger
BEFORE UPDATE ON categories
FOR EACH ROW
EXECUTE FUNCTION set_updated_at();

CREATE TABLE brands (
  "id" serial PRIMARY KEY NOT NULL,
  "name" varchar(64) UNIQUE NOT NULL,
  "image_url" varchar(256) NOT NULL,
  "updated_at" timestamptz NOT NULL DEFAULT now(),
  "created_at" timestamptz NOT NULL DEFAULT now()
);

CREATE TRIGGER brands_updated_at_trigger
BEFORE UPDATE ON brands
FOR EACH ROW
EXECUTE FUNCTION set_updated_at();


CREATE TABLE products (
  "id" bigserial PRIMARY KEY NOT NULL,
  "name" varchar(64) NOT NULL,
  "description" varchar(512),
  "image_url" varchar(256),
  "category_id" integer REFERENCES categories(id) ON DELETE CASCADE,
  "brand_id" integer NOT NULL REFERENCES brands(id) ON DELETE CASCADE,
  "price" decimal(10,5) NOT NULL,
  "updated_at" timestamptz NOT NULL DEFAULT now(),
  "created_at" timestamptz NOT NULL DEFAULT now()
);

CREATE INDEX products_name_idx ON products(name);

CREATE TRIGGER products_updated_at_trigger
BEFORE UPDATE ON products
FOR EACH ROW
EXECUTE FUNCTION set_updated_at();

CREATE TABLE countries (
  "alpha2" varchar(2) PRIMARY KEY NOT NULL,
  "name" varchar(64) NOT NULL,
  "updated_at" timestamptz NOT NULL DEFAULT now(),
  "created_at" timestamptz NOT NULL DEFAULT now()
);

CREATE TRIGGER countries_updated_at_trigger
BEFORE UPDATE ON countries
FOR EACH ROW
EXECUTE FUNCTION set_updated_at();

CREATE TABLE addresses (
  "id" serial PRIMARY KEY NOT NULL,
  "country" varchar(2) NOT NULL REFERENCES countries(alpha2) ON DELETE CASCADE,
  "state" varchar(32),
  "city" varchar(32) NOT NULL,
  "street" varchar(64) NOT NULL,
  "suite" varchar(8),
  "appartment" varchar(16) NOT NULL,
  "created_at" timestamptz NOT NULL DEFAULT now()
);

CREATE TABLE customers (
  "id" bigserial PRIMARY KEY NOT NULL,
  "first_name" varchar(32) NOT NULL,
  "last_name" varchar(32) NOT NULL,
  "email" varchar(128) NOT NULL,
  "phone_number" varchar(20),
  "birthdate" date NOT NULL,
  "gender" gender NOT NULL,
  "country_code" varchar(2) NOT NULL REFERENCES countries(alpha2) ON DELETE CASCADE,
  "joined_at" timestamptz NOT NULL DEFAULT now(),
  "updated_at" timestamptz NOT NULL DEFAULT now()
);

ALTER TABLE customers ADD CONSTRAINT customer_email_key UNIQUE (email);

CREATE TRIGGER customers_updated_at_trigger
BEFORE UPDATE ON customers
FOR EACH ROW
EXECUTE FUNCTION set_updated_at();


CREATE TABLE orders (
  "id" bigserial PRIMARY KEY NOT NULL,
  "status" order_status NOT NULL,
  "customer_id" bigint NOT NULL REFERENCES customers(id) ON DELETE CASCADE,
  "address_id" integer NOT NULL REFERENCES addresses(id) ON DELETE CASCADE,
  "created_at" timestamptz NOT NULL DEFAULT now(),
  "updated_at" timestamptz NOT NULL DEFAULT now()
);

CREATE INDEX orders_customer_id_idx ON orders(customer_id);
CREATE INDEX orders_created_at_idx ON orders(created_at);
CREATE INDEX orders_updated_at_idx ON orders(updated_at);

CREATE TRIGGER orders_updated_at_trigger
BEFORE UPDATE ON orders
FOR EACH ROW
EXECUTE FUNCTION set_updated_at();

CREATE TABLE order_lines (
  "order_id" bigint NOT NULL REFERENCES orders(id) ON DELETE CASCADE,
  "product_id" bigint NOT NULL REFERENCES products(id) ON DELETE CASCADE,
  "price" decimal(10,5) NOT NULL,
  "quantity" integer NOT NULL,
  "created_at" timestamptz NOT NULL DEFAULT now(),
  "updated_at" timestamptz NOT NULL DEFAULT now(),
  PRIMARY KEY ("order_id", "product_id")
);

CREATE INDEX order_lines_updated_at_idx ON order_lines(updated_at);

CREATE TRIGGER order_lines_updated_at_trigger
BEFORE UPDATE ON order_lines
FOR EACH ROW
EXECUTE FUNCTION set_updated_at();

CREATE TABLE message_types (
  "id" serial PRIMARY KEY NOT NULL,
  "name" varchar(16) NOT NULL UNIQUE
);

CREATE TABLE delivery_channels (
  "id" serial PRIMARY KEY NOT NULL,
  "name" varchar(16) NOT NULL UNIQUE
);

CREATE TABLE platforms (
  "id" serial PRIMARY KEY NOT NULL,
  "name" varchar(16) NOT NULL UNIQUE
);

CREATE TABLE topics (
  "id" serial PRIMARY KEY NOT NULL,
  "name" varchar(32) NOT NULL UNIQUE
);

CREATE TABLE campaigns (
  "id" bigserial PRIMARY KEY NOT NULL,
  "name" varchar(64) NOT NULL UNIQUE,
  "description" varchar(64),
  "type_id" integer NOT NULL REFERENCES message_types(id) ON DELETE CASCADE,
  "channel_id" integer NOT NULL REFERENCES delivery_channels(id) ON DELETE CASCADE,
  "topic_id" integer NOT NULL REFERENCES topics(id) ON DELETE CASCADE,
  "is_ab_test" boolean NOT NULL,
  "hour_sending_limit" integer NOT NULL,
  "subject_length" integer NOT NULL,
  "subject_with_personalization" boolean NOT NULL,
  "subject_with_deadline" boolean NOT NULL,
  "subject_with_emoji" boolean NOT NULL,
  "subject_with_bonuses" boolean NOT NULL,
  "subject_with_discount" boolean NOT NULL,
  "subject_with_saleout" boolean NOT NULL,
  "priority" integer NOT NULL DEFAULT 0,
  "started_at" timestamptz NOT NULL,
  "finished_at" timestamptz,
  "updated_at" timestamptz NOT NULL DEFAULT now()
);

CREATE TRIGGER campaigns_at_trigger
BEFORE UPDATE ON campaigns
FOR EACH ROW
EXECUTE FUNCTION set_updated_at();

CREATE TABLE messages (
  "id" varchar(36) PRIMARY KEY NOT NULL,
  "campaign_id" bigint NOT NULL REFERENCES campaigns(id) ON DELETE CASCADE,
  "customer_id" bigint NOT NULL REFERENCES customers(id) ON DELETE CASCADE,
  "platform_id" integer REFERENCES platforms(id) ON DELETE CASCADE,
  "opened_at" timestamptz,
  "clicked_at" timestamptz,
  "unsubscribed_at" timestamptz,
  "purchased_at" timestamptz,
  "order_id" bigint REFERENCES orders(id) ON DELETE CASCADE,
  "sent_at" timestamptz NOT NULL,
  "created_at" timestamptz NOT NULL DEFAULT now(),
  "updated_at" timestamptz NOT NULL DEFAULT now()
);

CREATE INDEX messages_updated_at_idx ON messages(updated_at);

CREATE TRIGGER messages_updated_at_trigger
BEFORE UPDATE ON messages
FOR EACH ROW
EXECUTE FUNCTION set_updated_at();

