-- CREATE TYPE "order_status" AS ENUM (
--   'CREATED',
--   'PAYED',
--   'READY_FOR_DELIVERY',
--   'DELIVERING',
--   'DELIVERED',
--   'CLOSED'
-- );

-- CREATE TYPE "gender" AS ENUM (
--   'MALE',
--   'FEMALE'
-- );

-- CREATE TABLE "categories" (
--   "id" serial PRIMARY KEY,
--   "name" varchar(64) NOT NULL,
--   "parent_id" integer,
--   "created_at" timestamptz NOT NULL DEFAULT 'now()'
-- );

-- CREATE TABLE "brands" (
--   "id" serial PRIMARY KEY NOT NULL,
--   "name" varchar(64) NOT NULL,
--   "image_url" varchar(256),
--   "created_at" timestamptz NOT NULL DEFAULT 'now()'
-- );

-- CREATE TABLE "products" (
--   "id" serial PRIMARY KEY,
--   "name" varchar(64) NOT NULL,
--   "description" varchar(512),
--   "image_url" varchar(256),
--   "category_id" integer NOT NULL,
--   "brand_id" integer NOT NULL,
--   "price" decimal(10,5) NOT NULL
-- );

-- CREATE TABLE "addresses" (
--   "id" serial PRIMARY KEY NOT NULL,
--   "country" varchar(2) NOT NULL,
--   "state" varchar(32),
--   "city" varchar(32) NOT NULL,
--   "street" varchar(32) NOT NULL,
--   "suite" varchat(8),
--   "appartment" varchar(16) NOT NULL
-- );

-- CREATE TABLE "orders" (
--   "id" serial PRIMARY KEY NOT NULL,
--   "status" order_status NOT NULL,
--   "customer_id" integer NOT NULL,
--   "address_id" integer NOT NULL,
--   "created_at" timestamptz NOT NULL DEFAULT 'now()',
--   "updated_at" timestamptz NOT NULL DEFAULT 'now()'
-- );

-- CREATE TABLE "order_lines" (
--   "order_id" integer,
--   "product_id" integer,
--   "price" decimal(10,5) NOT NULL,
--   "quantity" integer NOT NULL,
--   "created_at" timestamptz NOT NULL DEFAULT 'now()',
--   "updated_at" timestamptz NOT NULL DEFAULT 'now()',
--   PRIMARY KEY ("order_id", "product_id")
-- );

-- CREATE TABLE "countries" (
--   "alpha2" varchar(2) PRIMARY KEY NOT NULL,
--   "name" varchar(64) NOT NULL
-- );

-- CREATE TABLE "customers" (
--   "id" serial PRIMARY KEY,
--   "first_name" varchar(64) NOT NULL,
--   "last_name" varchar(64) NOT NULL,
--   "email" varchar(128) NOT NULL,
--   "phone_number" varchar(16),
--   "birthdate" date NOT NULL,
--   "gender" gender NOT NULL,
--   "country_code" varchar(2),
--   "joined_at" timestamptz NOT NULL DEFAULT 'now()',
--   "updated_at" timestamptz NOT NULL DEFAULT 'now()'
-- );

-- CREATE TABLE "message_types" (
--   "id" serial PRIMARY KEY NOT NULL,
--   "name" varchar(64) NOT NULL
-- );

-- CREATE TABLE "delivery_channels" (
--   "id" serial PRIMARY KEY NOT NULL,
--   "name" varchar(64) NOT NULL
-- );

-- CREATE TABLE "platforms" (
--   "id" serial PRIMARY KEY NOT NULL,
--   "name" varchar(64) NOT NULL
-- );

-- CREATE TABLE "topics" (
--   "id" serial PRIMARY KEY NOT NULL,
--   "name" varchar(64) NOT NULL
-- );

-- CREATE TABLE "campaigns" (
--   "id" serial PRIMARY KEY NOT NULL,
--   "type_id" integer NOT NULL,
--   "channel_id" integer NOT NULL,
--   "topic_id" integer NOT NULL,
--   "is_ab_test" bool NOT NULL,
--   "hour_sending_limit" integer NOT NULL,
--   "subject_length" integer NOT NULL,
--   "subject_with_personalization" bool NOT NULL,
--   "subject_with_deadline" bool NOT NULL,
--   "subject_with_emoji" bool NOT NULL,
--   "subject_with_bonuses" bool NOT NULL,
--   "subject_with_discount" bool NOT NULL,
--   "subject_with_saleout" bool NOT NULL,
--   "priority" integer,
--   "started_at" timestamptz NOT NULL,
--   "finished_at" timestamptz NOT NULL
-- );

-- CREATE TABLE "messages" (
--   "id" bigint PRIMARY KEY NOT NULL,
--   "campaign_id" integer NOT NULL,
--   "customer_id" integer NOT NULL,
--   "platform_id" integer,
--   "opened_at" timestamptz,
--   "clicked_at" timestamptz,
--   "unsubscribed_at" timestamptz,
--   "order_id" integer,
--   "sent_at" timestamptz NOT NULL,
--   "created_at" timestamptz NOT NULL DEFAULT 'now()',
--   "updated_at" timestamptz NOT NULL DEFAULT 'now()'
-- );

-- ALTER TABLE "categories" ADD FOREIGN KEY ("id") REFERENCES "categories" ("parent_id");
-- ALTER TABLE "products" ADD FOREIGN KEY ("category_id") REFERENCES "categories" ("id");
-- ALTER TABLE "products" ADD FOREIGN KEY ("brand_id") REFERENCES "brands" ("id");
-- ALTER TABLE "addresses" ADD FOREIGN KEY ("country") REFERENCES "countries" ("alpha2");
-- ALTER TABLE "orders" ADD FOREIGN KEY ("customer_id") REFERENCES "customers" ("id");
-- ALTER TABLE "orders" ADD FOREIGN KEY ("address_id") REFERENCES "addresses" ("id");
-- ALTER TABLE "order_lines" ADD FOREIGN KEY ("order_id") REFERENCES "orders" ("id");
-- ALTER TABLE "order_lines" ADD FOREIGN KEY ("product_id") REFERENCES "products" ("id");
-- ALTER TABLE "customers" ADD FOREIGN KEY ("country_code") REFERENCES "countries" ("alpha2");
-- ALTER TABLE "campaigns" ADD FOREIGN KEY ("type_id") REFERENCES "message_types" ("id");
-- ALTER TABLE "campaigns" ADD FOREIGN KEY ("channel_id") REFERENCES "delivery_channels" ("id");
-- ALTER TABLE "campaigns" ADD FOREIGN KEY ("topic_id") REFERENCES "topics" ("id");
-- ALTER TABLE "messages" ADD FOREIGN KEY ("campaign_id") REFERENCES "campaigns" ("id");
-- ALTER TABLE "messages" ADD FOREIGN KEY ("customer_id") REFERENCES "customers" ("id");
-- ALTER TABLE "messages" ADD FOREIGN KEY ("platform_id") REFERENCES "platforms" ("id");
-- ALTER TABLE "messages" ADD FOREIGN KEY ("order_id") REFERENCES "orders" ("id");
