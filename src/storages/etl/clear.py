from ..clickhouse.utils import get_client
from .components.age_groups import clear_age_groups
from .components.campaigns import clear_campaigns
from .components.categories import clear_categories
from .components.countries import clear_countries
from .components.customers import clear_customers
from .components.date import clear_date
from .components.messages import clear_messages
from .components.products import clear_products
from .components.sales import clear_sales
from .components.subjects import clear_subjects
from .components.time import clear_time


def main():
    clickhoust_client = get_client()
    clear_age_groups(clickhoust_client)
    clear_countries(clickhoust_client)
    clear_customers(clickhoust_client)
    clear_time(clickhoust_client)
    clear_date(clickhoust_client)
    clear_categories(clickhoust_client)
    clear_products(clickhoust_client)
    clear_sales(clickhoust_client)
    clear_subjects(clickhoust_client)
    clear_campaigns(clickhoust_client)
    clear_messages(clickhoust_client)


if __name__ == "__main__":
    main()
