from collections import defaultdict

from clickhouse_connect.driver.client import Client
from psycopg import Connection
from psycopg.rows import dict_row

from .schemas import Category


def get_categories(conn: Connection) -> list[Category]:
    with conn.cursor(row_factory=dict_row) as cursor:
        cursor.execute(
            """	
            WITH RECURSIVE cte AS (
                SELECT 
                    id, 
                    parent_id, 
                    name,
                    name::text AS full_name,
                    null as parent_full_name,
                    false as is_bottom,
                    true as is_top
                FROM categories
                WHERE parent_id is NULL

                UNION ALL

                SELECT 
                    c.id,
                    c.parent_id,
                    c.name,
                    (cte.full_name || '.' || c.name)::text,
                    cte.full_name as parent_full_name,
                    not exists(
                        select 1 
                        from categories c3 
                        where c3.parent_id = c.id
                    ) as is_bottom,
                    c.parent_id is null as is_top
                FROM categories c
                JOIN cte on c.parent_id = cte.id
            )
            SELECT *
            FROM cte
            """
        )
        results = cursor.fetchall()

    return [
        Category(
            oltpKey=r["id"],
            name=r["full_name"],
            parent_name=r["parent_full_name"],
            parent_oltpKey=r["parent_id"],
            distance=r["full_name"].count(".") + 1,
            is_bottom=r["is_bottom"],
            is_top=r["is_top"],
        )
        for r in results
    ]


def insert_categories(
    clickhouse_client: Client,
    records: list[Category],
) -> int:
    records = sorted(records, key=lambda obj: obj.distance)
    categories_by_distance: dict[int, list[Category]] = defaultdict(list)
    key_counter = 1

    for record in records:
        categories_by_distance[record.distance].append(record)

    for _, categories in categories_by_distance.items():
        parent_oltp_keys = [
            c.parent_oltpKey for c in categories if c.parent_oltpKey is not None
        ]

        result = clickhouse_client.query(
            """
            SELECT Key, OltpKey
            FROM DimCategories
            WHERE OltpKey IN {keys:Array(UInt64)}
            """,
            parameters={"keys": list(set(parent_oltp_keys))},
        )

        category_oltp_to_category_key: dict[int, str] = {
            oltp_key: key for key, oltp_key in result.result_rows
        }

        clickhouse_client.insert(
            table="DimCategories",
            data=[
                [
                    str(key_counter + i),
                    str(category.oltpKey),
                    (
                        "n/a"
                        if category.parent_oltpKey is None
                        else category_oltp_to_category_key[
                            category.parent_oltpKey
                        ]
                    ),
                    category.name,
                    category.parent_name or "n/a",
                    category.distance,
                    category.is_bottom,
                    category.is_top,
                ]
                for i, category in enumerate(categories)
            ],
            column_names=[
                "Key",
                "OltpKey",
                "ParentKey",
                "Name",
                "ParentName",
                "Distance",
                "IsBottomFlag",
                "IsTopFlag",
            ],
        )

        key_counter += len(records)

    return len(records)


def clear_categories(clickhouse_client: Client):
    clickhouse_client.command("DELETE FROM DimCategories WHERE true")
