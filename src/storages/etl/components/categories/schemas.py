from pydantic import BaseModel


class Category(BaseModel):
    oltpKey: int
    name: str
    parent_name: str | None
    parent_oltpKey: int | None
    distance: int
    is_bottom: bool
    is_top: bool
