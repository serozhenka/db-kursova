from clickhouse_connect.driver.client import Client
from psycopg import Connection
from psycopg.rows import dict_row

from .schemas import Product


def get_products(conn: Connection) -> list[Product]:
    with conn.cursor(row_factory=dict_row) as cursor:
        cursor.execute(
            """	
            select p.id, p.name, b.name as brand_name, p.category_id, round(p.price, 2) as price
            from products p
            join brands b on p.brand_id = b.id
            """
        )
        results = cursor.fetchall()

    return [
        Product(
            oltp_key=r["id"],
            name=r["name"],
            brand_name=r["brand_name"],
            category_oltp_key=r["category_id"],
            price=r["price"],
        )
        for r in results
    ]


def insert_products(
    clickhouse_client: Client,
    records: list[Product],
) -> int:
    result = clickhouse_client.query(
        """
        SELECT OltpKey, Key
        FROM DimCategories
        WHERE OltpKey IN {keys:Array(UInt64)}
        """,
        parameters={
            "keys": list(
                set(
                    r.category_oltp_key
                    for r in records
                    if r.category_oltp_key is not None
                )
            )
        },
    )
    category_oltp_to_key: dict[int, str] = {
        oltp_key: key for oltp_key, key in result.result_rows
    }

    clickhouse_client.insert(
        table="DimProducts",
        data=[
            [
                str(i + 1),
                product.oltp_key,
                product.name,
                product.brand_name,
                (
                    category_oltp_to_key[product.category_oltp_key]
                    if product.category_oltp_key is not None
                    else "n/a"
                ),
                product.price,
            ]
            for i, product in enumerate(records)
        ],
        column_names=[
            "Key",
            "OltpKey",
            "Name",
            "BrandName",
            "CategoryKey",
            "Price",
        ],
    )

    return len(records)


def clear_products(clickhouse_client: Client):
    clickhouse_client.command("DELETE FROM DimProducts WHERE true")
