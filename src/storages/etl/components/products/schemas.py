from decimal import Decimal

from pydantic import BaseModel


class Product(BaseModel):
    oltp_key: int
    name: str
    brand_name: str
    category_oltp_key: int | None
    price: Decimal
