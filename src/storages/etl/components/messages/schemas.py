from datetime import datetime
from decimal import Decimal

from pydantic import BaseModel


class Message(BaseModel):
    campaignKeyOltp: int
    productKeyOltp: int | None
    platform_name: str | None
    ts: datetime
    clicked_times: int
    opened_times: int
    sent_times: int
    unsubscribed_times: int
    purchased_times: int
    total_price: Decimal
