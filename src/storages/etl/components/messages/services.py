from datetime import date

from clickhouse_connect.driver.client import Client
from psycopg import Connection

from ..time import Time
from .schemas import Message


def get_messages(conn: Connection) -> list[Message]:
    with conn.cursor() as cursor:
        cursor.execute(
            """	
            SELECT 
                campaign_id,
                ol.product_id,
                p.name,
                date_trunc('minute', m.updated_at),
                c.subject_length,
                count(*) FILTER (WHERE clicked_at IS NOT NULL) as clicked_times,
                count(*) FILTER (WHERE opened_at IS NOT NULL) as opened_times,
                count(*) FILTER (WHERE sent_at IS NOT NULL) as sent_times,
                count(*) FILTER (WHERE unsubscribed_at IS NOT NULL) as unsubscribed_times,
                count(*) FILTER (WHERE o.id IS NOT NULL) as purchased_times,
                coalesce(round(sum(ol.price), 2), 0)
            FROM messages m
            LEFT JOIN orders o ON O.id = m.order_id
            LEFT JOIN order_lines ol ON o.id = ol.order_id
            LEFT JOIN platforms p ON p.id = m.platform_id
            JOIN campaigns c ON c.id = m.campaign_id
            GROUP BY 
                campaign_id,
                ol.product_id,
                p.name,
                date_trunc('minute', m.updated_at),
                c.subject_length
            """
        )
        results = cursor.fetchall()

    return [
        Message(
            campaignKeyOltp=r[0],
            productKeyOltp=r[1],
            platform_name=r[2],
            ts=r[3],
            clicked_times=r[5],
            opened_times=r[6],
            sent_times=r[7],
            unsubscribed_times=r[8],
            purchased_times=r[9],
            total_price=r[10],
        )
        for r in results
    ]


def insert_messages(
    clickhouse_client: Client,
    records: list[Message],
) -> int:
    result = clickhouse_client.query(
        """
        SELECT DateKey, Date
        FROM DimDate
        """
    )
    date_to_date_key: dict[date, str] = {
        date: date_key for date_key, date in result.result_rows
    }

    result = clickhouse_client.query(
        """
        SELECT TimeKey, Minute, Hour
        FROM DimTime
        """
    )
    time_to_time_key: dict[Time, str] = {
        Time(minute=minute, hour=hour): time_key
        for time_key, minute, hour in result.result_rows
    }

    result = clickhouse_client.query(
        """
        SELECT OltpKey, Key
        FROM DimCampaigns
        WHERE OltpKey IN {keys:Array(UInt64)}
        """,
        parameters={"keys": list(set(r.campaignKeyOltp for r in records))},
    )
    campaign_oltp_to_key: dict[int, str] = {
        oltp_key: key for oltp_key, key in result.result_rows
    }

    result = clickhouse_client.query(
        """
        SELECT OltpKey, Key
        FROM DimProducts
        WHERE OltpKey IN {keys:Array(UInt64)}
        """,
        parameters={
            "keys": list(
                set(
                    r.productKeyOltp
                    for r in records
                    if r.productKeyOltp is not None
                )
            )
        },
    )
    product_oltp_to_key: dict[int, int] = {
        oltp_key: key for oltp_key, key in result.result_rows
    }

    clickhouse_client.insert(
        table="FactMessages",
        data=[
            [
                campaign_oltp_to_key[campaign.campaignKeyOltp],
                (
                    product_oltp_to_key[campaign.productKeyOltp]
                    if campaign.productKeyOltp is not None
                    else "n/a"
                ),
                campaign_oltp_to_key[campaign.campaignKeyOltp],
                campaign.platform_name or "n/a",
                date_to_date_key[campaign.ts.date()],
                time_to_time_key[
                    Time(minute=campaign.ts.minute, hour=campaign.ts.hour)
                ],
                campaign.clicked_times,
                campaign.opened_times,
                campaign.sent_times,
                campaign.unsubscribed_times,
                campaign.purchased_times,
                campaign.total_price,
            ]
            for campaign in records
        ],
        column_names=[
            "CampaignKey",
            "ProductKey",
            "SubjectKey",
            "PlatformName",
            "DateKey",
            "TimeKey",
            "ClickedTimes",
            "OpenedTimes",
            "SentTimes",
            "UnsubscribedTimes",
            "PurchasedTimes",
            "TotalPrice",
        ],
    )
    return len(records)


def clear_messages(clickhouse_client: Client):
    clickhouse_client.command("DELETE FROM FactMessages WHERE true")
