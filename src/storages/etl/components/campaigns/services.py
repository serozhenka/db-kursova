from datetime import date

from clickhouse_connect.driver.client import Client
from psycopg import Connection
from psycopg.rows import dict_row

from .schemas import Campaign


def get_campaigns(conn: Connection) -> list[Campaign]:
    with conn.cursor(row_factory=dict_row) as cursor:
        cursor.execute(
            """	
            select
                c.id,
                c.name,
                mt.name as type_name,
                dc.name as channel_name,
                t.name as topic_name,
                c.started_at,
                c.is_ab_test,
                c.subject_length
            from campaigns c
            join message_types mt on mt.id = c.type_id
            join delivery_channels dc on dc.id = c.channel_id
            join topics t on t.id = c.topic_id
            """
        )
        results = cursor.fetchall()

    return [
        Campaign(
            oltp_key=r["id"],
            name=r["name"],
            type_name=r["type_name"],
            channel_name=r["channel_name"],
            topic_name=r["topic_name"],
            ts=r["started_at"],
            is_ab_test=r["is_ab_test"],
            subject_length=r["subject_length"],
        )
        for r in results
    ]


def insert_campaigns(
    clickhouse_client: Client,
    records: list[Campaign],
) -> int:
    result = clickhouse_client.query(
        """
        SELECT DateKey, Date
        FROM DimDate
        """
    )
    date_to_date_key: dict[date, str] = {
        date: date_key for date_key, date in result.result_rows
    }

    clickhouse_client.insert(
        table="DimCampaigns",
        data=[
            [
                i + 1,
                campaign.oltp_key,
                campaign.name,
                campaign.type_name,
                campaign.channel_name,
                campaign.topic_name,
                date_to_date_key[campaign.ts.date()],
                campaign.is_ab_test,
            ]
            for i, campaign in enumerate(records)
        ],
        column_names=[
            "Key",
            "OltpKey",
            "Name",
            "TypeName",
            "ChannelName",
            "TopicName",
            "DateKey",
            "IsAbTest",
        ],
    )
    return len(records)


def clear_campaigns(clickhouse_client: Client):
    clickhouse_client.command("DELETE FROM DimCampaigns WHERE true")
