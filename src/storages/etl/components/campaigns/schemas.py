from datetime import datetime

from pydantic import BaseModel


class Campaign(BaseModel):
    oltp_key: int
    name: str
    type_name: str
    channel_name: str
    topic_name: str
    ts: datetime
    is_ab_test: bool
    subject_length: int
