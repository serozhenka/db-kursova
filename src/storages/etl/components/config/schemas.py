from datetime import datetime

from pydantic import BaseModel


class EtlConfig(BaseModel):
    is_initial: bool = True
    last_etl_time: datetime | None = None
