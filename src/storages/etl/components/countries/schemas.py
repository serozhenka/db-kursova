from pydantic import BaseModel


class Country(BaseModel, frozen=True):
    alpha2: str
    name: str
