from datetime import datetime

from clickhouse_connect.driver.client import Client
from psycopg import Connection

from .schemas import Country


def get_countries(conn: Connection) -> list[Country]:
    with conn.cursor() as cursor:
        cursor.execute(
            """	
            SELECT alpha2, name
            FROM countries
            """
        )
        results = cursor.fetchall()

    return [Country(alpha2=r[0], name=r[1]) for r in results]


def get_updated_countries(conn: Connection, from_ts: datetime) -> list[Country]:
    with conn.cursor() as cursor:
        cursor.execute(
            """	
            SELECT alpha2, name
            FROM countries
            WHERE countries.updated_at >= %(from_ts)s
            """,
            params={"from_ts": from_ts},
        )
        results = cursor.fetchall()

    return [Country(alpha2=r[0], name=r[1]) for r in results]


def insert_countries(
    clickhouse_client: Client,
    records: list[Country],
) -> int:
    clickhouse_client.insert(
        table="DimLocations",
        data=[
            [i + 1, "n/a", "n/a", country.alpha2, country.name]
            for i, country in enumerate(set(records))
        ],
        column_names=[
            "Key",
            "CityName",
            "StateName",
            "CountryAlpha2",
            "CountryName",
        ],
    )
    return len(set(records))


def insert_or_update_countries(
    clickhouse_client: Client,
    records: list[Country],
) -> tuple[int, int]:
    inserted, updated = 0, 0

    for record in records:
        result = clickhouse_client.query(
            """
            SELECT Key
            FROM DimLocations
            WHERE
                CityName = 'n/a'
                AND StateName = 'n/a'
                AND CountryAlpha2 = {alpha2:String(2)}
            """,
            parameters={"alpha2": record.alpha2},
        )

        if result.result_rows:  # update row
            location_key: int = result.result_rows[0][0]
            clickhouse_client.query(
                """
                ALTER TABLE DimLocations
                UPDATE CountryName = {name:String(64)}
                WHERE Key = {key:UInt32}
                """,
                parameters={"key": location_key, "name": record.name},
            )
            updated += 1
        else:  # insert row
            clickhouse_client.insert(
                table="DimLocations",
                data=[
                    [i + 1, "n/a", "n/a", country.alpha2, country.name]
                    for i, country in enumerate(set(records))
                ],
                column_names=[
                    "Key",
                    "CityName",
                    "StateName",
                    "CountryAlpha2",
                    "CountryName",
                ],
            )
            inserted += 1

    return inserted, updated


def clear_countries(clickhouse_client: Client):
    clickhouse_client.command("DELETE FROM DimLocations WHERE true")
