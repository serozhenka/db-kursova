from pydantic import BaseModel, computed_field


class AgeGroup(BaseModel):
    start_age: int
    end_age: int

    @computed_field
    @property
    def name(self) -> str:
        return f"{self.start_age}_{self.end_age}"
