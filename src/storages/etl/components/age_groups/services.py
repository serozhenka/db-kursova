from clickhouse_connect.driver.client import Client

from .schemas import AgeGroup


def get_age_groups() -> list[AgeGroup]:
    return [AgeGroup(start_age=i, end_age=i + 10) for i in range(0, 100, 10)]


def insert_age_groups(
    clickhouse_client: Client,
    age_groups: list[AgeGroup],
) -> None:
    clickhouse_client.insert(
        table="DimAgeGroups",
        data=[
            [i + 1, ag.name, ag.start_age, ag.end_age]
            for i, ag in enumerate(age_groups)
        ],
        column_names=["Key", "Name", "StartAge", "EndAge"],
    )


def clear_age_groups(clickhouse_client: Client):
    clickhouse_client.command("DELETE FROM DimAgeGroups WHERE true")
