from pydantic import BaseModel


class Subject(BaseModel, frozen=True):
    oltp_key: int
    length: int
    with_personalization: bool
    with_deadline: bool
    with_emoji: bool
    with_bonuses: bool
    with_discount: bool
    with_saleout: bool
