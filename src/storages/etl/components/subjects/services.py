from clickhouse_connect.driver.client import Client
from psycopg import Connection
from psycopg.rows import dict_row

from .schemas import Subject


def get_subjects(conn: Connection) -> list[Subject]:
    with conn.cursor(row_factory=dict_row) as cursor:
        cursor.execute(
            """	
            select
                id,
                subject_length,
                subject_with_personalization,
                subject_with_deadline,
                subject_with_emoji,
                subject_with_bonuses,
                subject_with_discount,
                subject_with_saleout
            from campaigns
            """
        )
        results = cursor.fetchall()

    return [
        Subject(
            oltp_key=r["id"],
            length=r["subject_length"],
            with_personalization=r["subject_with_personalization"],
            with_deadline=r["subject_with_deadline"],
            with_emoji=r["subject_with_emoji"],
            with_bonuses=r["subject_with_bonuses"],
            with_discount=r["subject_with_discount"],
            with_saleout=r["subject_with_saleout"],
        )
        for r in results
    ]


def insert_subjects(
    clickhouse_client: Client,
    records: list[Subject],
) -> int:
    clickhouse_client.insert(
        table="DimSubjects",
        data=[
            [
                i + 1,
                subject.oltp_key,
                subject.length,
                subject.with_personalization,
                subject.with_deadline,
                subject.with_emoji,
                subject.with_bonuses,
                subject.with_discount,
                subject.with_saleout,
            ]
            for i, subject in enumerate(set(records))
        ],
        column_names=[
            "Key",
            "OltpKey",
            "Length",
            "WithPersonalization",
            "WithDeadline",
            "WithEmoji",
            "WithBonuses",
            "WithDiscount",
            "WithSaleout",
        ],
    )
    return len(set(records))


def clear_subjects(clickhouse_client: Client):
    clickhouse_client.command("DELETE FROM DimSubjects WHERE true")
