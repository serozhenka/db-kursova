from datetime import datetime

from dateutil.relativedelta import relativedelta
from pydantic import BaseModel


class Customer(BaseModel):
    oltpKey: int
    birthdate: datetime
    gender: str
    country_alpha2: str
    country_name: str

    @property
    def age_group_name(self) -> str:
        age = relativedelta(datetime.now(), self.birthdate).years
        age_tens = age // 10 * 10
        return f"{age_tens}_{age_tens + 10}"
