from datetime import datetime

from clickhouse_connect.driver.client import Client
from psycopg import Connection

from .schemas import Customer


def get_customers(conn: Connection) -> list[Customer]:
    with conn.cursor() as cursor:
        cursor.execute(
            """	
            SELECT id, birthdate, gender, countries.name, countries.alpha2
            FROM customers
            JOIN countries ON customers.country_code = countries.alpha2
            """
        )
        results = cursor.fetchall()

    return [
        Customer(
            oltpKey=r[0],
            birthdate=r[1],
            gender=r[2][0].upper(),
            country_name=r[3],
            country_alpha2=r[4],
        )
        for r in results
    ]


def get_updated_customers(
    conn: Connection, from_ts: datetime
) -> list[Customer]:
    with conn.cursor() as cursor:
        cursor.execute(
            """	
            SELECT id, birthdate, gender, countries.name, countries.alpha2
            FROM customers
            JOIN countries ON customers.country_code = countries.alpha2
            WHERE customers.updated_at >= %(from_ts)s
            """,
            params={"from_ts": from_ts},
        )
        results = cursor.fetchall()

    return [
        Customer(
            oltpKey=r[0],
            birthdate=r[1],
            gender=r[2][0].upper(),
            country_name=r[3],
            country_alpha2=r[4],
        )
        for r in results
    ]


def insert_customers(
    clickhouse_client: Client,
    records: list[Customer],
) -> int:
    countries = list(set(c.country_name for c in records))
    result = clickhouse_client.query(
        """
        SELECT Key, CountryName
        FROM DimLocations
        WHERE 
            CityName = 'n/a'
            AND StateName = 'n/a'
            AND CountryName IN {countries:Array(String(64))}
        """,
        parameters={"countries": countries},
    )
    country_name_to_location_key: dict[str, int] = {
        country_name: location_key
        for location_key, country_name in result.result_rows
    }

    result = clickhouse_client.query(
        """
        SELECT Key, Name
        FROM DimAgeGroups
        """
    )
    age_group_name_to_key: dict[str, int] = {
        name: age_group_key for age_group_key, name in result.result_rows
    }

    clickhouse_client.insert(
        table="DimCustomers",
        data=[
            [
                i + 1,
                customer.oltpKey,
                customer.birthdate,
                age_group_name_to_key[customer.age_group_name],
                customer.gender,
                country_name_to_location_key[customer.country_name],
                True,
            ]
            for i, customer in enumerate(records)
        ],
        column_names=[
            "Key",
            "OltpKey",
            "Birthdate",
            "AgeGroupKey",
            "Gender",
            "LocationKey",
            "IsActive",
        ],
    )

    return len(records)


def insert_or_update_customers(
    clickhouse_client: Client,
    records: list[Customer],
) -> tuple[int, int]:
    inserted, updated = 0, 0

    for customer in records:
        to_insert = True

        result = clickhouse_client.query(
            """
            SELECT DimLocations.CountryName
            FROM DimCustomers
            JOIN DimLocations ON DimCustomers.LocationKey = DimLocations.Key
            WHERE OltpKey = {key:UInt64}
            """,
            parameters={"key": customer.oltpKey},
        )

        if result.result_rows:  # update row
            country_name = result.result_rows[0][0]

            if country_name != customer.country_name:
                clickhouse_client.query(
                    """
                    ALTER TABLE DimCustomers
                    UPDATE IsActive = false
                    WHERE OltpKey = {key:UInt64}
                    """,
                    parameters={"key": customer.oltpKey},
                )
                updated += 1
            else:
                to_insert = False

        if not to_insert:
            continue

        age_group_key_result = clickhouse_client.query(
            """
            SELECT Key, Name
            FROM DimAgeGroups
            WHERE Name = {name:String(32)}
            """,
            parameters={"name": customer.age_group_name},
        )

        location_key_result = clickhouse_client.query(
            """
            SELECT Key, CountryName
            FROM DimLocations
            WHERE 
                CityName = 'n/a'
                AND StateName = 'n/a'
                AND CountryName = {country_name:String(64)}
            """,
            parameters={"country_name": customer.country_name},
        )

        clickhouse_client.insert(
            table="DimCustomers",
            data=[
                [
                    i + 1,
                    customer.oltpKey,
                    customer.birthdate,
                    age_group_key_result.result_rows[0][0],
                    customer.gender,
                    location_key_result.result_rows[0][0],
                    True,
                ]
                for i, customer in enumerate(records)
            ],
            column_names=[
                "Key",
                "OltpKey",
                "Birthdate",
                "AgeGroupKey",
                "Gender",
                "LocationKey",
                "IsActive",
            ],
        )
        inserted += 1

    return inserted, updated


def clear_customers(clickhouse_client: Client):
    clickhouse_client.command("DELETE FROM DimCustomers WHERE true")
