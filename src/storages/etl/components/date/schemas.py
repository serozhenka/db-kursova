from datetime import date

from pydantic import BaseModel


class Date(BaseModel):
    day: int
    month: int
    year: int

    @property
    def dt(self) -> date:
        return date(self.year, self.month, self.day)

    @property
    def day_of_the_week(self) -> str:
        weekday = self.dt.weekday()
        return {
            0: "Monday",
            1: "Tuesday",
            2: "Wednesday",
            3: "Thursday",
            4: "Friday",
            5: "Saturday",
            6: "Sunday",
        }[weekday]

    @property
    def month_name(self) -> str:
        month_name = self.dt.strftime("%B")
        return f"{month_name} {self.year}"
