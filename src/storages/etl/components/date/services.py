from datetime import date, datetime, timedelta, timezone

from clickhouse_connect.driver.client import Client

from .schemas import Date


def daterange(start_date, end_date):
    for n in range(int((end_date - start_date).days)):
        yield start_date + timedelta(n)


def get_date() -> list[Date]:
    start_date = date(2020, 1, 1)
    end_date = date.today() + timedelta(days=1)

    return [
        Date(day=dt.day, month=dt.month, year=dt.year)
        for dt in daterange(start_date, end_date)
    ]


def get_new_dates(from_ts: datetime) -> list[Date]:
    start_date = from_ts.date() + timedelta(days=1)
    end_date = datetime.now(tz=timezone.utc).date() + timedelta(days=1)

    return [
        Date(day=dt.day, month=dt.month, year=dt.year)
        for dt in daterange(start_date, end_date)
    ]


def insert_date(
    clickhouse_client: Client,
    records: list[Date],
) -> int:
    result = clickhouse_client.query("SELECT MAX(DateKey) FROM DimDate")
    max_date_key = result.result_rows[0][0] if result.result_rows else 0

    clickhouse_client.insert(
        table="DimDate",
        data=[
            [
                max_date_key + i + 1,
                dt.dt,
                dt.day_of_the_week,
                dt.month_name,
                dt.year,
            ]
            for i, dt in enumerate(records)
        ],
        column_names=["DateKey", "Date", "DayOfTheWeek", "Month", "Year"],
    )

    return len(records)


def clear_date(clickhouse_client: Client):
    clickhouse_client.command("DELETE FROM DimDate WHERE true")
