from clickhouse_connect.driver.client import Client

from .schemas import Time


def get_time() -> list[Time]:
    return [
        Time(minute=minute, hour=hour)
        for hour in range(0, 24)
        for minute in range(0, 60)
    ]


def insert_time(
    clickhouse_client: Client,
    records: list[Time],
) -> int:
    clickhouse_client.insert(
        table="DimTime",
        data=[
            [
                i + 1,
                time.minute,
                time.hour,
            ]
            for i, time in enumerate(records)
        ],
        column_names=["TimeKey", "Minute", "Hour"],
    )

    return len(records)


def clear_time(clickhouse_client: Client):
    clickhouse_client.command("DELETE FROM DimTime WHERE true")
