from pydantic import BaseModel


class Time(BaseModel, frozen=True):
    minute: int
    hour: int
