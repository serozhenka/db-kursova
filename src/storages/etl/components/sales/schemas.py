from datetime import datetime
from decimal import Decimal

from pydantic import BaseModel

from ..locations import Location


class Sale(BaseModel):
    oltpKey: int
    productOltpKey: int
    customerOltpKey: int
    location: Location
    ts: datetime
    quantity: int
    price: Decimal
