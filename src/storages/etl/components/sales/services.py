from datetime import date, datetime

from clickhouse_connect.driver.client import Client
from psycopg import Connection

from ...utils import chunkify
from ..locations import Location
from ..time import Time
from .schemas import Sale


def get_sales(conn: Connection, from_ts: datetime = datetime.min) -> list[Sale]:
    with conn.cursor() as cursor:
        cursor.execute(
            """	
            select 
            	orders.id,
                ol.product_id,
                orders.customer_id,
                a.city,
                a.state,
                c.name,
                ol.created_at,
                round(ol.price, 2),
                ol.quantity,
                c.alpha2
            from order_lines ol
            join orders on ol.order_id = orders.id
            join addresses a on a.id = orders.address_id
            join countries c on c.alpha2 = a.country
            where ol.updated_at >= %(from_ts)s
            """,
            params={"from_ts": from_ts},
        )
        results = cursor.fetchall()

    return [
        Sale(
            oltpKey=r[0],
            productOltpKey=r[1],
            customerOltpKey=r[2],
            location=Location(
                city_name=r[3] or "n/a",
                state_name=r[4] or "n/a",
                country_alpha2=r[9] or "n/a",
                country_name=r[5] or "n/a",
            ),
            ts=r[6],
            price=r[7],
            quantity=r[8],
        )
        for r in results
    ]


def _insert_sales(
    clickhouse_client: Client,
    records: list[Sale],
) -> int:
    result = clickhouse_client.query(
        """
        SELECT OltpKey, Key
        FROM DimProducts
        WHERE OltpKey IN {keys:Array(UInt64)}
        """,
        parameters={
            "keys": list(
                set(
                    r.productOltpKey
                    for r in records
                    if r.productOltpKey is not None
                )
            )
        },
    )
    product_oltp_to_key: dict[int, str] = {
        oltp_key: key for oltp_key, key in result.result_rows
    }

    result = clickhouse_client.query(
        """
        SELECT OltpKey, Key
        FROM DimCustomers
        WHERE OltpKey IN {keys:Array(UInt64)}
        """,
        parameters={
            "keys": list(
                set(
                    r.customerOltpKey
                    for r in records
                    if r.customerOltpKey is not None
                )
            )
        },
    )
    customer_oltp_to_key: dict[int, str] = {
        oltp_key: key for oltp_key, key in result.result_rows
    }

    result = clickhouse_client.query(
        """
        SELECT max(Key)
        FROM DimLocations
        """
    )
    max_location_id: int = result.result_rows[0][0]
    locations_ids: dict[Location, int] = {
        sale.location: max_location_id + i + 1 for i, sale in enumerate(records)
    }

    clickhouse_client.insert(
        table="DimLocations",
        data=[
            [
                locations_ids[location],
                location.city_name,
                location.state_name,
                location.country_alpha2,
                location.country_name,
            ]
            for location in set(sale.location for sale in records)
        ],
        column_names=[
            "Key",
            "CityName",
            "StateName",
            "CountryAlpha2",
            "CountryName",
        ],
    )

    result = clickhouse_client.query(
        """
        SELECT DateKey, Date
        FROM DimDate
        """
    )
    date_to_date_key: dict[date, str] = {
        date: date_key for date_key, date in result.result_rows
    }

    result = clickhouse_client.query(
        """
        SELECT TimeKey, Minute, Hour
        FROM DimTime
        """
    )
    time_to_time_key: dict[Time, str] = {
        Time(minute=minute, hour=hour): time_key
        for time_key, minute, hour in result.result_rows
    }

    clickhouse_client.insert(
        table="FactSales",
        data=[
            [
                sale.oltpKey,
                product_oltp_to_key[sale.productOltpKey],
                customer_oltp_to_key[sale.customerOltpKey],
                locations_ids[sale.location],
                date_to_date_key[sale.ts.date()],
                time_to_time_key[
                    Time(minute=sale.ts.minute, hour=sale.ts.hour)
                ],
                sale.quantity,
                sale.price,
            ]
            for sale in records
        ],
        column_names=[
            "SaleOltpKey",
            "ProductKey",
            "CustomerKey",
            "LocationKey",
            "DateKey",
            "TimeKey",
            "Quantity",
            "Price",
        ],
    )

    return len(records)


def insert_sales(
    clickhouse_client: Client,
    records: list[Sale],
) -> int:
    for records_chunk in chunkify(records, 5_000):
        _insert_sales(clickhouse_client, records=records_chunk)

    return len(records)


def _insert_or_update_sales(
    clickhouse_client: Client,
    records: list[Sale],
) -> tuple[int, int]:
    oltp_keys = set(r.oltpKey for r in records)
    result = clickhouse_client.query(
        """
        SELECT SaleOltpKey FROM FactSales
        WHERE SaleOltpKey IN {keys:Array(UInt64)}
        """,
        parameters={"keys": list(oltp_keys)},
    )

    existing_ids = set(r[0] for r in result.result_rows)
    unexisting_ids = oltp_keys.difference(existing_ids)

    insert_sales(
        clickhouse_client=clickhouse_client,
        records=[r for r in records if r.oltpKey in unexisting_ids],
    )

    for record in [r for r in records if r.oltpKey in existing_ids]:
        clickhouse_client.query(
            """
                ALTER TABLE FactSales
                UPDATE Quantity = {quantity:UInt32}, Price = {price:Decimal64(2)}
                WHERE SaleOltpKey = {key:UInt64}
                """,
            parameters={
                "key": record.oltpKey,
                "quantity": record.quantity,
                "price": record.price,
            },
        )

    return len(unexisting_ids), len(existing_ids)


def insert_or_update_sales(
    clickhouse_client: Client,
    records: list[Sale],
) -> tuple[int, int]:
    inserted, updated = 0, 0
    for records_chunk in chunkify(records, 5_000):
        _inserted, _updated = _insert_or_update_sales(
            clickhouse_client,
            records=records_chunk,
        )
        inserted += _inserted
        updated += _updated

    return inserted, updated


def clear_sales(clickhouse_client: Client):
    clickhouse_client.command("DELETE FROM FactSales WHERE true")
