from pydantic import BaseModel


class Location(BaseModel, frozen=True):
    city_name: str
    state_name: str
    country_alpha2: str
    country_name: str
