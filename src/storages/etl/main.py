from datetime import datetime, timezone
from typing import cast

from psycopg import Connection

from storages.etl.components.date.services import get_new_dates
from storages.etl.components.sales.services import insert_or_update_sales
from storages.settings import settings

from ..clickhouse.utils import get_client
from .components.age_groups import get_age_groups, insert_age_groups
from .components.campaigns import get_campaigns, insert_campaigns
from .components.categories import get_categories, insert_categories
from .components.countries import (
    get_countries,
    get_updated_countries,
    insert_countries,
    insert_or_update_countries,
)
from .components.customers import (
    get_customers,
    get_updated_customers,
    insert_customers,
    insert_or_update_customers,
)
from .components.date import get_date, insert_date
from .components.messages import get_messages, insert_messages
from .components.products import get_products, insert_products
from .components.sales import get_sales, insert_sales
from .components.subjects import get_subjects, insert_subjects
from .components.time import get_time, insert_time
from .utils import get_config, save_config


def main():
    config = get_config()

    clickhouse_client = get_client()
    conn = Connection.connect(settings.db_url)

    if config.is_initial:  # initial ETL
        age_groups = get_age_groups()
        insert_age_groups(clickhouse_client, age_groups)
        yield f"Inserted {len(age_groups)} age groups"

        countries = get_countries(conn)
        inserted = insert_countries(clickhouse_client, countries)
        yield f"Inserted {inserted} countries"

        customers = get_customers(conn)
        inserted = insert_customers(clickhouse_client, customers)
        yield f"Inserted {inserted} customers"

        time = get_time()
        inserted = insert_time(clickhouse_client, time)
        yield f"Inserted {inserted} time records"

        date = get_date()
        inserted = insert_date(clickhouse_client, date)
        yield f"Inserted {inserted} dates"

        categories = get_categories(conn)
        inserted = insert_categories(clickhouse_client, categories)
        yield f"Inserted {inserted} categories"

        products = get_products(conn)
        inserted = insert_products(clickhouse_client, products)
        yield f"Inserted {inserted} products"

        sales = get_sales(conn)
        inserted = insert_sales(clickhouse_client, sales)
        yield f"Inserted {inserted} sales"

        subjects = get_subjects(conn)
        inserted = insert_subjects(clickhouse_client, subjects)
        yield f"Inserted {inserted} subjects"

        campaigns = get_campaigns(conn)
        inserted = insert_campaigns(clickhouse_client, campaigns)
        yield f"Inserted {inserted} campaigns"

        messages = get_messages(conn)
        inserted = insert_messages(clickhouse_client, messages)
        yield f"Inserted {inserted} messages"
    else:
        last_etl_time = cast(datetime, config.last_etl_time)

        new_dates = get_new_dates(last_etl_time)
        yield f"Got {len(new_dates)} new dates"
        i = insert_date(clickhouse_client, new_dates)
        yield f"Inserted {i} dates"

        updated_countries = get_updated_countries(conn, last_etl_time)
        yield f"Got {len(updated_countries)} new or updated countries"
        i, u = insert_or_update_countries(clickhouse_client, updated_countries)
        yield f"Inserted {i} countries"
        yield f"Updated {u} countries"

        updated_customers = get_updated_customers(conn, last_etl_time)
        yield f"Got {len(updated_customers)} new or updated customers"
        i, u = insert_or_update_customers(clickhouse_client, updated_customers)
        yield f"Inserted {i} customers"
        yield f"Updated {u} customers"

        updated_sales = get_sales(conn, from_ts=last_etl_time)
        yield f"Got {len(updated_sales)} new or updated sales"
        i, u = insert_or_update_sales(clickhouse_client, updated_sales)
        yield f"Inserted {i} sales"
        yield f"Updated {u} sales"

    config.is_initial = False
    config.last_etl_time = datetime.now(timezone.utc)
    save_config(config)
