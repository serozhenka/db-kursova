import os
from typing import Generator, TypeVar

from .components.config import EtlConfig

T = TypeVar("T")


def get_config() -> EtlConfig:
    config_path = os.path.join(os.path.dirname(__file__), "etl_config.json")
    if not os.path.exists(config_path):
        return EtlConfig()

    with open(config_path) as file:
        return EtlConfig.model_validate_json(file.read())


def save_config(config: EtlConfig) -> None:
    config_path = os.path.join(os.path.dirname(__file__), "etl_config.json")
    with open(config_path, "w") as file:
        file.write(config.model_dump_json(indent=4))


def delete_config() -> None:
    config_path = os.path.join(os.path.dirname(__file__), "etl_config.json")
    try:
        os.remove(config_path)
    except OSError:
        pass


def chunkify(iterable: list[T], n: int) -> Generator[list[T], None, None]:
    for i in range(0, len(iterable), n):
        yield iterable[i : i + n]
