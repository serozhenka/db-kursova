from fastapi import APIRouter
from sse_starlette import EventSourceResponse
from starlette import status
from starlette.responses import JSONResponse, StreamingResponse

from storages.application.exceptions import ClientError

from .services import (
    drop_database,
    drop_storage,
    fill_database,
    fill_storage,
    init_database,
    init_storage,
)

router = APIRouter(prefix="/database")


@router.get("/init")
async def inti_database_route():
    try:
        init_database()
    except ClientError as e:
        return JSONResponse(
            content={"error": str(e)},
            status_code=status.HTTP_400_BAD_REQUEST,
        )


@router.get("/drop")
async def drop_database_route():
    try:
        drop_database()
    except ClientError as e:
        return JSONResponse(
            content={"error": str(e)},
            status_code=status.HTTP_400_BAD_REQUEST,
        )


@router.get("/fill")
async def fill_database_route():
    return EventSourceResponse(fill_database())


@router.get("/init-storage")
async def init_storage_route():
    try:
        init_storage()
    except ClientError as e:
        return JSONResponse(
            content={"error": str(e)},
            status_code=status.HTTP_400_BAD_REQUEST,
        )


@router.get("/drop-storage")
async def drop_storage_route():
    try:
        drop_storage()
    except ClientError as e:
        return JSONResponse(
            content={"error": str(e)},
            status_code=status.HTTP_400_BAD_REQUEST,
        )


@router.get("/fill-storage")
async def fill_storage_route():
    return EventSourceResponse(fill_storage())
