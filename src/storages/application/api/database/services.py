import time

import psycopg
from clickhouse_connect.driver.exceptions import DatabaseError

from storages.application.exceptions import ClientError
from storages.clickhouse.delete import delete as drop_olap
from storages.clickhouse.init import init as init_olap
from storages.etl.main import main as etl
from storages.etl.utils import delete_config
from storages.postgres.delete import main as delete
from storages.postgres.init import main as init
from storages.seeding.main import main as seed


def init_database():
    try:
        init()
    except psycopg.errors.DuplicateObject:
        raise ClientError("Database is already initialized")


def drop_database():
    try:
        delete()
    except psycopg.errors.UndefinedTable:
        raise ClientError("Nothing to drop")


def fill_database():
    try:
        for message in seed():
            yield {"event": "update", "data": message}
    except Exception:
        yield {"event": "error", "data": "Database is already filled"}

    yield {"event": "update", "data": "Succesfully filled the database"}
    yield {"event": "close", "data": ""}


def init_storage():
    try:
        init_olap()
    except DatabaseError:
        raise ClientError("Storage is already initialized")


def drop_storage():
    try:
        drop_olap()
        delete_config()
    except DatabaseError:
        raise ClientError("Nothing to drop")


def fill_storage():
    for message in etl():
        yield {"event": "update", "data": message}
        time.sleep(0.5)

    yield {"event": "update", "data": "ETL process finished successfully"}
    yield {"event": "close", "data": ""}
