from datetime import date
from typing import Annotated

from fastapi import APIRouter, Depends, Path, Request
from starlette.templating import Jinja2Templates

from storages.application.api.products.services import get_product_names
from storages.application.db.tables.orders import OrderStatus
from storages.application.state import DatabaseConnection, get_templates

from ..countries.services import get_countries
from .services import get_campaign, list_campaigns

router = APIRouter(prefix="/campaigns")


@router.get("/")
async def list_orders_route(
    request: Request,
    conn: DatabaseConnection,
    name: str | None = None,
    start_date: date | None = None,
    end_date: date | None = None,
    limit: int = 10,
    offset: int = 0,
    templates: Jinja2Templates = Depends(get_templates),
):
    campaigns = await list_campaigns(
        conn=conn,
        name=name,
        start_date=start_date,
        end_date=end_date,
        limit=limit + 1,
        offset=offset,
    )

    return templates.TemplateResponse(
        name="campaigns.html",
        context={
            "request": request,
            "campaigns": campaigns[:limit],
            "name": name,
            "start_date": start_date,
            "end_date": end_date,
            "limit": limit,
            "offset": offset,
            "has_next": len(campaigns) > limit,
            "has_prev": offset > 0,
        },
    )


# @router.post("/")
# async def insert_order_route(conn: DatabaseConnection, model: InsertOrder):
#     try:
#         order_id = await insert_order(conn=conn, model=model)
#     except ClientError as e:
#         return JSONResponse(
#             content={"error": str(e)},
#             status_code=status.HTTP_400_BAD_REQUEST,
#         )

#     return JSONResponse(content={"id": str(order_id)})


@router.get("/{id}")
async def get_campaign_route(
    request: Request,
    conn: DatabaseConnection,
    id_: Annotated[int, Path(alias="id")],
    templates: Jinja2Templates = Depends(get_templates),
):
    campaign = await get_campaign(conn=conn, id_=id_)
    return templates.TemplateResponse(
        name="campaign_detail.html",
        context={"request": request, "campaign": campaign},
    )


# @router.put("/{id}", status_code=status.HTTP_204_NO_CONTENT)
# async def update_order_route(
#     conn: DatabaseConnection,
#     id_: Annotated[int, Path(alias="id")],
#     model: UpdateOrder,
# ):
#     await update_order(conn=conn, id_=id_, model=model)
