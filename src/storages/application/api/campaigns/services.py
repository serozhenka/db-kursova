from datetime import date

from pydantic import TypeAdapter
from sqlalchemy import func, sql
from sqlalchemy.ext.asyncio import AsyncConnection

from storages.application.db.tables.campaigns import Campaign as CampaignTable
from storages.application.db.tables.campaigns import (
    DeliveryChannel as DeliveryChannelTable,
)
from storages.application.db.tables.campaigns import (
    MessageType as MessageTypeTable,
)
from storages.application.db.tables.campaigns import Topic as TopicTable
from storages.application.db.tables.messages import Message as MessageTable

from .schemas import Campaign, CampaignDetail


async def list_campaigns(
    conn: AsyncConnection,
    name: str | None = None,
    start_date: date | None = None,
    end_date: date | None = None,
    limit: int = 10,
    offset: int = 0,
):
    query = (
        sql.select(
            CampaignTable.id,
            CampaignTable.name,
            CampaignTable.started_at,
            CampaignTable.finished_at,
            (
                func.count(sql.literal(1))
                .filter(MessageTable.opened_at.is_not(None))
                .label("opened")
            ),
            (
                func.count(sql.literal(1))
                .filter(MessageTable.clicked_at.is_not(None))
                .label("clicked")
            ),
            (
                func.count(sql.literal(1))
                .filter(MessageTable.sent_at.is_not(None))
                .label("sent")
            ),
            (
                func.count(sql.literal(1))
                .filter(MessageTable.order_id.is_not(None))
                .label("purchased")
            ),
        )
        .join(
            MessageTable,
            MessageTable.campaign_id == CampaignTable.id,
            isouter=True,
        )
        .group_by(CampaignTable.id)
        .order_by(CampaignTable.started_at.desc())
        .limit(limit)
        .offset(offset)
    )

    if start_date:
        query = query.where(CampaignTable.started_at >= start_date)

    if end_date:
        query = query.where(CampaignTable.started_at <= end_date)

    if name:
        query = query.where(
            func.lower(CampaignTable.name).startswith(name.lower())
        )

    result = await conn.execute(query)
    return TypeAdapter(list[Campaign]).validate_python(result.mappings().all())


async def get_campaign(conn: AsyncConnection, id_: int) -> CampaignDetail:
    query = (
        sql.select(
            CampaignTable.id,
            CampaignTable.name,
            CampaignTable.description,
            MessageTypeTable.name.label("type"),
            TopicTable.name.label("topic"),
            DeliveryChannelTable.name.label("channel"),
            CampaignTable.is_ab_test,
            CampaignTable.hour_sending_limit,
            CampaignTable.subject_length,
            CampaignTable.subject_with_personalization,
            CampaignTable.subject_with_deadline,
            CampaignTable.subject_with_emoji,
            CampaignTable.subject_with_bonuses,
            CampaignTable.subject_with_discount,
            CampaignTable.subject_with_saleout,
            CampaignTable.priority,
            CampaignTable.started_at,
            CampaignTable.finished_at,
            CampaignTable.updated_at,
            (
                func.count(sql.literal(1))
                .filter(MessageTable.opened_at.is_not(None))
                .label("opened")
            ),
            (
                func.count(sql.literal(1))
                .filter(MessageTable.clicked_at.is_not(None))
                .label("clicked")
            ),
            (
                func.count(sql.literal(1))
                .filter(MessageTable.sent_at.is_not(None))
                .label("sent")
            ),
            (
                func.count(sql.literal(1))
                .filter(MessageTable.order_id.is_not(None))
                .label("purchased")
            ),
        )
        .join(
            MessageTable,
            MessageTable.campaign_id == CampaignTable.id,
            isouter=True,
        )
        .join(MessageTypeTable, MessageTypeTable.id == CampaignTable.type_id)
        .join(TopicTable, TopicTable.id == CampaignTable.topic_id)
        .join(
            DeliveryChannelTable,
            DeliveryChannelTable.id == CampaignTable.channel_id,
        )
        .where(CampaignTable.id == id_)
        .group_by(
            CampaignTable.id,
            MessageTypeTable.id,
            TopicTable.id,
            DeliveryChannelTable.id,
        )
    )

    result = await conn.execute(query)
    return TypeAdapter(CampaignDetail).validate_python(result.mappings().one())
