from datetime import date
from typing import Annotated

from fastapi import APIRouter, Depends, Path, Request
from fastapi.responses import JSONResponse
from starlette import status
from starlette.templating import Jinja2Templates

from storages.application.api.products.services import get_product_names
from storages.application.db.tables.orders import OrderStatus
from storages.application.exceptions import ClientError
from storages.application.state import DatabaseConnection, get_templates

from ..countries.services import get_countries
from .schemas import InsertOrder, UpdateOrder
from .services import get_order, insert_order, list_orders, update_order

router = APIRouter(prefix="/orders")


@router.get("/")
async def list_orders_route(
    request: Request,
    conn: DatabaseConnection,
    order_id: int | None = None,
    customer_email: str | None = None,
    status: OrderStatus | None = None,
    start_date: date | None = None,
    end_date: date | None = None,
    limit: int = 10,
    offset: int = 0,
    templates: Jinja2Templates = Depends(get_templates),
):
    orders = await list_orders(
        conn=conn,
        order_id=order_id,
        customer_email=customer_email,
        status=status,
        start_date=start_date,
        end_date=end_date,
        limit=limit + 1,
        offset=offset,
    )
    countries = await get_countries(conn)
    products = await get_product_names(conn)

    return templates.TemplateResponse(
        name="orders.html",
        context={
            "request": request,
            "orders": orders[:limit],
            "available_countries": countries,
            "order_id": order_id,
            "customer_email": customer_email,
            "status": status,
            "available_statuses": [os.value for os in OrderStatus],
            "available_products": products,
            "start_date": start_date,
            "end_date": end_date,
            "limit": limit,
            "offset": offset,
            "has_next": len(orders) > limit,
            "has_prev": offset > 0,
        },
    )


@router.post("/")
async def insert_order_route(conn: DatabaseConnection, model: InsertOrder):
    try:
        order_id = await insert_order(conn=conn, model=model)
    except ClientError as e:
        return JSONResponse(
            content={"error": str(e)},
            status_code=status.HTTP_400_BAD_REQUEST,
        )

    return JSONResponse(content={"id": str(order_id)})


@router.get("/{id}")
async def get_order_route(
    request: Request,
    conn: DatabaseConnection,
    id_: Annotated[int, Path(alias="id")],
    templates: Jinja2Templates = Depends(get_templates),
):
    order = await get_order(conn=conn, id_=id_)
    return templates.TemplateResponse(
        name="order_detail.html",
        context={
            "request": request,
            "order": order,
            "available_statuses": [os.value for os in OrderStatus],
        },
    )


@router.put("/{id}", status_code=status.HTTP_204_NO_CONTENT)
async def update_order_route(
    conn: DatabaseConnection,
    id_: Annotated[int, Path(alias="id")],
    model: UpdateOrder,
):
    await update_order(conn=conn, id_=id_, model=model)
