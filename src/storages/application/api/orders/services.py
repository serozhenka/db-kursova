from datetime import date

from psycopg import IntegrityError
from pydantic import TypeAdapter
from sqlalchemy import func, sql
from sqlalchemy.dialects.postgresql import insert
from sqlalchemy.ext.asyncio import AsyncConnection
from sqlalchemy.orm import aliased

from storages.application.db.tables.customers import Customer as CustomerTable
from storages.application.db.tables.locations import Address, Country
from storages.application.db.tables.orders import Order as OrderTable
from storages.application.db.tables.orders import OrderLine as OrderLineTable
from storages.application.db.tables.orders import OrderStatus
from storages.application.db.tables.products import Product as ProductTable
from storages.application.db.utils import json_build_object
from storages.application.exceptions import ClientError, NotFoundException

from .schemas import InsertOrder, Order, OrderDetail, UpdateOrder


async def list_orders(
    conn: AsyncConnection,
    order_id: int | None = None,
    customer_email: str | None = None,
    status: OrderStatus | None = None,
    start_date: date | None = None,
    end_date: date | None = None,
    limit: int = 10,
    offset: int = 0,
) -> list[Order]:
    query = (
        sql.select(
            OrderTable,
            CustomerTable.email.label("customer_email"),
        )
        .join(CustomerTable, CustomerTable.id == OrderTable.customer_id)
        .order_by(OrderTable.created_at.desc())
        .limit(limit)
        .offset(offset)
    )

    if order_id is not None:
        query = query.where(OrderTable.id == order_id)

    if customer_email is not None:
        query = query.where(CustomerTable.email.startswith(customer_email))

    if status is not None:
        query = query.where(OrderTable.status == status)

    if start_date is not None:
        query = query.where(OrderTable.created_at >= start_date)

    if end_date is not None:
        query = query.where(OrderTable.created_at <= end_date)

    result = await conn.execute(query)
    return TypeAdapter(list[Order]).validate_python(result.mappings().all())


async def get_order(
    conn: AsyncConnection,
    id_: int | None = None,
) -> OrderDetail:
    OrderCountry = aliased(Country)

    query = (
        sql.select(
            OrderTable.id,
            OrderTable.status,
            json_build_object(
                {
                    "id": CustomerTable.id,
                    "first_name": CustomerTable.first_name,
                    "last_name": CustomerTable.last_name,
                    "email": CustomerTable.email,
                    "gender": CustomerTable.gender,
                    "country": Country.name,
                }
            ).label("customer"),
            json_build_object(
                {
                    "country": OrderCountry.name,
                    "state": Address.state,
                    "city": Address.city,
                    "street": Address.street,
                    "suite": Address.suite,
                    "appartment": Address.appartment,
                }
            ).label("address"),
            func.array_agg(
                json_build_object(
                    {
                        "product_name": ProductTable.name,
                        "price": OrderLineTable.price,
                        "quantity": OrderLineTable.quantity,
                    }
                )
            ).label("order_lines"),
            OrderTable.created_at,
            OrderTable.updated_at,
        )
        .join(CustomerTable, CustomerTable.id == OrderTable.customer_id)
        .join(Country, CustomerTable.country_code == Country.alpha2)
        .join(Address, OrderTable.address_id == Address.id)
        .join(OrderCountry, Address.country == OrderCountry.alpha2)
        .join(OrderLineTable, OrderLineTable.order_id == OrderTable.id)
        .join(ProductTable, ProductTable.id == OrderLineTable.product_id)
        .where(OrderTable.id == id_)
        .group_by(
            OrderTable.id,
            CustomerTable.id,
            Country.alpha2,
            OrderCountry.alpha2,
            Address.id,
        )
    )

    result = await conn.execute(query)

    if not (obj := result.mappings().one_or_none()):
        raise NotFoundException()

    result = await conn.execute(query)
    return OrderDetail.model_validate(obj)


async def insert_order(conn: AsyncConnection, model: InsertOrder) -> int:
    result = await conn.execute(
        sql.select(CustomerTable.id).where(
            CustomerTable.email == model.customer_email
        )
    )
    if not (customer_id := result.scalar_one_or_none()):
        raise ClientError("No customers match given email")

    result = await conn.execute(
        insert(Address)
        .values(
            country=(
                sql.select(Country.alpha2)
                .where(Country.name == model.country)
                .limit(1)
            ),
            state=model.state,
            city=model.city,
            street=model.street,
            appartment=model.appartment,
        )
        .returning(Address.id)
    )
    address_id = result.scalar_one()

    result = await conn.execute(
        insert(OrderTable)
        .values(
            status=OrderStatus.CREATED,
            customer_id=customer_id,
            address_id=address_id,
        )
        .returning(OrderTable.id)
    )
    order_id = result.scalar_one()
    print(order_id)

    try:
        query = sql.insert(OrderLineTable).values(
            [
                {
                    "order_id": order_id,
                    "product_id": (
                        sql.select(ProductTable.id)
                        .where(ProductTable.name == order_line.product_name)
                        .limit(1)
                    ),
                    "price": (
                        sql.select(ProductTable.price * order_line.quantity)
                        .where(ProductTable.name == order_line.product_name)
                        .limit(1)
                    ),
                    "quantity": order_line.quantity,
                }
                for order_line in model.order_lines
            ]
        )
        await conn.execute(query)
    except (IntegrityError, Exception) as e:
        await conn.rollback()
        raise ClientError("Invalid product name") from e
    else:
        await conn.commit()

    return order_id


async def update_order(
    conn: AsyncConnection,
    id_: int,
    model: UpdateOrder,
) -> None:
    query = (
        sql.update(OrderTable)
        .values(status=model.status)
        .where(OrderTable.id == id_)
    )
    await conn.execute(query)

    for order_line in model.order_lines:
        product_subquery = (
            sql.select(ProductTable.id, ProductTable.price)
            .where(ProductTable.name == order_line.product_name)
            .limit(1)
            .subquery()
        )

        query = (
            sql.update(OrderLineTable)
            .values(
                price=product_subquery.c.price * order_line.quantity,
                quantity=order_line.quantity,
            )
            .where(
                OrderLineTable.order_id == id_,
                OrderLineTable.product_id == product_subquery.c.id,
            )
        )

        await conn.execute(query)

    await conn.commit()
