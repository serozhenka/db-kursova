from datetime import datetime
from decimal import Decimal
from typing import Annotated

from annotated_types import Ge
from pydantic import BaseModel

from storages.application.db.tables.orders import OrderStatus

from ..locations.schemas import Address


class Order(BaseModel):
    id: int
    status: OrderStatus
    customer_id: int
    customer_email: str
    address_id: int
    created_at: datetime
    updated_at: datetime


class OrderCustomer(BaseModel):
    id: int
    first_name: str
    last_name: str
    email: str
    gender: str
    country: str


class OrderLine(BaseModel):
    product_name: str
    price: Decimal
    quantity: Annotated[int, Ge(1)]


class OrderDetail(BaseModel):
    id: int
    status: OrderStatus
    customer: OrderCustomer
    address: Address
    order_lines: list[OrderLine]
    created_at: datetime
    updated_at: datetime


class InsertUpdateOrderLine(BaseModel):
    product_name: str
    quantity: Annotated[int, Ge(1)]


class InsertOrder(BaseModel):
    customer_email: str
    country: str
    state: str | None
    city: str
    street: str
    appartment: str
    order_lines: list[InsertUpdateOrderLine]


class UpdateOrder(BaseModel):
    status: OrderStatus
    order_lines: list[InsertUpdateOrderLine]
