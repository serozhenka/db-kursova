from pydantic import BaseModel


class Address(BaseModel):
    country: str
    state: str | None
    city: str
    street: str
    suite: str | None
    appartment: str
