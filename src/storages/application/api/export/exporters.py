import csv
import json
from enum import StrEnum
from io import StringIO
from test.test_re import B
from typing import Any, Protocol, Sequence, cast, final

from dicttoxml import dicttoxml


class ExporterType(StrEnum):
    CSV = "csv"
    JSON = "json"
    XML = "xml"


class Exporter(Protocol):
    def export(
        self,
        column_names: Sequence[str],
        data: Sequence[Sequence[Any]],
    ) -> StringIO: ...


@final
class CsvExporter(Exporter):
    def export(
        self,
        column_names: Sequence[str],
        data: Sequence[Sequence[Any]],
    ) -> StringIO:
        writable = StringIO()
        writer = csv.writer(writable)
        writer.writerow(column_names)
        writer.writerows(data)
        writable.seek(0)
        return writable


@final
class JsonExporter(Exporter):
    def export(
        self,
        column_names: Sequence[str],
        data: Sequence[Sequence[Any]],
    ) -> StringIO:
        writable = StringIO()
        object_list = [dict(zip(column_names, obj)) for obj in data]
        writable.write(json.dumps(object_list, default=str))
        writable.seek(0)
        return writable


@final
class XmlExporter(Exporter):
    def export(
        self,
        column_names: Sequence[str],
        data: Sequence[Sequence[Any]],
    ) -> StringIO:
        writable = StringIO()
        object_list = [dict(zip(column_names, obj)) for obj in data]
        xml = dicttoxml(
            object_list,
            custom_root="root",
            attr_type=False,
            return_bytes=False,
        )
        writable.write(cast(str, xml))
        writable.seek(0)
        return writable


def exporter_factory(exporter_type: ExporterType) -> Exporter:
    return {
        ExporterType.CSV: CsvExporter,
        ExporterType.JSON: JsonExporter,
        ExporterType.XML: XmlExporter,
    }[exporter_type]()
