import os
from io import StringIO, TextIOWrapper
from typing import Any, cast
from xml.etree import ElementTree
from xml.etree.ElementTree import Element

from storages.clickhouse.utils import clickhouse_client

from .exporters import Exporter
from .schemas import (
    Cube,
    Dimension,
    GlobalDimension,
    Hierarchy,
    Join,
    Level,
    Measure,
    Table,
)


def get_attr(element: Element, name: str) -> str:
    return element.attrib[name]


def get_olap_cubes() -> list[Cube]:
    schema = os.path.join(os.path.dirname(__file__), "mondrian_schema.xml")
    with open(schema) as file:
        tree = ElementTree.parse(file)
        root = tree.getroot()

    global_dimensions: list[GlobalDimension] = []
    cubes: list[Cube] = []

    def get_element(element: Element | Any):
        nonlocal global_dimensions

        if element.tag == "Cube":
            measures: list[Measure] = []
            dimensions: list[Dimension] = []
            table: Table  # pyright: ignore

            for child in element:
                child = get_element(child)

                if isinstance(child, Measure):
                    measures.append(child)
                elif isinstance(child, Table):
                    table = child
                elif isinstance(child, Dimension):
                    dimensions.append(child)

            return Cube(
                name=get_attr(element, "name"),
                table=table,
                measures=measures,
                dimensions=dimensions,
            )
        elif element.tag == "Measure":
            return Measure(
                name=get_attr(element, "name"),
                column=get_attr(element, "column"),
            )
        elif element.tag == "CalculatedMember":
            pass
        elif element.tag == "Table":
            return Table(name=get_attr(element, "name"))
        elif element.tag == "Dimension":
            hierarchies: list[Hierarchy] = []

            for child in element:
                child = get_element(child)
                if isinstance(child, Hierarchy):
                    hierarchies.append(child)

            if not element.get("foreignKey"):
                return GlobalDimension(
                    name=get_attr(element, "name"),
                    hierarchies=hierarchies,
                )
            else:
                return Dimension(
                    name=get_attr(element, "name"),
                    foreign_key=get_attr(element, "foreignKey"),
                    hierarchies=hierarchies,
                )
        elif element.tag == "Hierarchy":
            levels: list[Level] = []
            table: Table | None = None
            join: Join | None = None

            for child in element:
                child = get_element(child)
                if isinstance(child, Table):
                    table = child
                elif isinstance(child, Level):
                    levels.append(child)
                elif isinstance(child, Join):
                    join = child
                    table = Table(name=get_attr(element, "primaryKeyTable"))

            return Hierarchy(
                name=get_attr(element, "name"),
                primary_key=element.get("primaryKey"),
                table=table,
                join=join,
                levels=levels,
            )
        elif element.tag == "Level":
            return Level(
                name=get_attr(element, "name"),
                column=get_attr(element, "column"),
            )
        elif element.tag == "Join":
            tables: list[Table] = []

            for child in element:
                child = get_element(child)
                if isinstance(child, Table):
                    tables.append(child)

            return Join(
                right_table=tables[-1],
                left_column=get_attr(element, "leftKey"),
                right_column=get_attr(element, "rightKey"),
            )
        elif element.tag == "DimensionUsage":
            global_dimension = next(
                gd
                for gd in global_dimensions
                if gd.name == get_attr(element, "source")
            )
            return Dimension(
                name=global_dimension.name,
                foreign_key=get_attr(element, "foreignKey"),
                hierarchies=global_dimension.hierarchies,
            )

        else:
            raise Exception("Invalid tag")

    for child in root:
        element = get_element(child)
        if isinstance(element, GlobalDimension):
            global_dimensions.append(element)
        elif isinstance(element, Cube):
            cubes.append(element)

    return cubes


def cube_to_clickhouse_query(cube: Cube, limit: int | None = None) -> str:
    to_select = [
        m.to_selectable(table=cube.table.name, column_prefix=cube.name)
        for m in cube.measures
        if m.is_active
    ]
    selected_from: list[Table] = []
    joins: list[str] = []
    selected_from.append(cube.table)

    for dimension in cube.dimensions:
        if not dimension.is_active:
            continue

        for hierarchy in dimension.hierarchies:
            if not hierarchy.is_active:
                continue

            if hierarchy.join:
                join = hierarchy.join
                primary_table = cast(Table, hierarchy.table)
                primary_alias = (
                    f"{primary_table}{selected_from.count(primary_table) + 1}"
                )
                join_primary = f"JOIN {primary_table} {primary_alias} ON {cube.table}.{dimension.foreign_key} = {primary_alias}.{hierarchy.primary_key}"

                child_table = hierarchy.join.right_table
                alias = f"{child_table}{selected_from.count(child_table) + 1}"
                join_child = f"JOIN {child_table} {alias} ON {primary_alias}.{join.left_column} = {alias}.{join.right_column}"

                for level in hierarchy.levels:
                    if not level.is_active:
                        continue

                    to_select.append(
                        level.to_selectable(
                            table=alias,
                            column_prefix=hierarchy.name,
                        )
                    )

                joins.extend([join_primary, join_child])
                selected_from.extend([primary_table, child_table])

            elif hierarchy.table:
                if hierarchy.table == cube.table:
                    for level in hierarchy.levels:
                        if not level.is_active:
                            continue

                        to_select.append(
                            level.to_selectable(
                                table=cube.table.name,
                                column_prefix=hierarchy.name,
                            )
                        )
                    continue

                alias = f"{hierarchy.table}{selected_from.count(hierarchy.table) + 1}"
                table_name = str(hierarchy.table)
                join_exp = f"JOIN {table_name} {alias} ON {cube.table}.{dimension.foreign_key} = {alias}.{hierarchy.primary_key}"

                for level in hierarchy.levels:
                    if not level.is_active:
                        continue

                    to_select.append(
                        level.to_selectable(
                            table=alias,
                            column_prefix=hierarchy.name,
                        )
                    )

                joins.append(join_exp)
                selected_from.append(hierarchy.table)

    query = f"SELECT {', '.join(to_select)} FROM {str(cube.table)}"
    concat = [query] + joins
    if limit is not None:
        concat.append(f"LIMIT {limit}")

    return "\n".join(concat)


def export_cube(
    exporter: Exporter,
    cube: Cube,
    limit: int | None = None,
) -> StringIO:
    query = cube_to_clickhouse_query(cube=cube, limit=limit)
    client = clickhouse_client()
    results = client.query(query)
    return exporter.export(
        column_names=results.column_names,
        data=results.result_rows,
    )
