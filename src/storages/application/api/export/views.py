from typing import Annotated

from fastapi import APIRouter, Depends, Path, Request, Response
from starlette import status
from starlette.responses import StreamingResponse
from starlette.templating import Jinja2Templates

from storages.application.state import get_templates

from .exporters import ExporterType, exporter_factory
from .schemas import ExportRequest
from .services import export_cube, get_olap_cubes

router = APIRouter(prefix="/export")


@router.get("/")
async def export_page(
    request: Request,
    templates: Jinja2Templates = Depends(get_templates),
):
    cubes = get_olap_cubes()
    return templates.TemplateResponse(
        name="export.html",
        context={
            "request": request,
            "exporters": [str(exp) for exp in ExporterType],
            "cubes": [c.model_dump(mode="json") for c in cubes],
        },
    )


@router.get("/cubes")
async def list_cubes():
    cubes = get_olap_cubes()
    return [cube.name for cube in cubes]


@router.get("/cubes/{name}")
async def get_cube(name: Annotated[str, Path()]):
    cubes = get_olap_cubes()
    try:
        cube = next(cube for cube in cubes if cube.name.lower() == name.lower())
    except StopIteration:
        return Response(
            content="No cube matches given name",
            status_code=status.HTTP_400_BAD_REQUEST,
        )

    return cube


@router.post("/make")
async def make_export(request: ExportRequest):
    exporter = exporter_factory(exporter_type=request.exporter_type)
    object = export_cube(
        exporter=exporter,
        cube=request.cube,
        limit=request.limit,
    )
    return StreamingResponse(
        object,
        media_type="application/octet-stream",
        headers={"Content-Disposition": "filename=export.csv"},
    )
