from pydantic import BaseModel

from .exporters import ExporterType


class Activatable(BaseModel):
    is_active: bool = True


class Level(Activatable):
    table: str | None = None
    name: str
    column: str

    def to_selectable(self, table: str, column_prefix: str) -> str:
        return f"{table}.{self.column} AS {column_prefix}_{self.name}"


class Measure(Activatable):
    name: str
    column: str

    def to_selectable(self, table: str, column_prefix: str) -> str:
        return f"{table}.{self.column} AS {column_prefix}_{self.name}"


class Table(BaseModel, frozen=True):
    name: str
    alias: str | None = None

    def __str__(self):
        return self.alias or self.name


class Join(BaseModel):
    right_table: Table
    left_column: str
    right_column: str


class Hierarchy(Activatable):
    name: str
    primary_key: str | None
    table: Table | None = None
    join: Join | None = None
    levels: list[Level]


class GlobalDimension(BaseModel):
    name: str
    hierarchies: list[Hierarchy]


class Dimension(Activatable):
    name: str
    foreign_key: str
    hierarchies: list[Hierarchy]


class Cube(BaseModel):
    name: str
    table: Table
    measures: list[Measure]
    dimensions: list[Dimension]


class ExportRequest(BaseModel):
    cube: Cube
    exporter_type: ExporterType
    limit: int | None = None
