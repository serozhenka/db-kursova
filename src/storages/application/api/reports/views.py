from fastapi import APIRouter, Depends, Request
from starlette.templating import Jinja2Templates

from storages.application.state import get_templates

router = APIRouter(prefix="/reports")


@router.get("/")
async def report_page(
    request: Request,
    templates: Jinja2Templates = Depends(get_templates),
):
    return templates.TemplateResponse(
        name="report.html",
        context={"request": request},
    )
