from sqlalchemy import sql
from sqlalchemy.ext.asyncio import AsyncConnection

from storages.application.db.tables.locations import Country


async def get_countries(conn: AsyncConnection) -> list[str]:
    query = sql.select(Country.name).order_by(Country.name)
    result = await conn.execute(query)
    return list(result.scalars().all())
