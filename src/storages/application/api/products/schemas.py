from decimal import Decimal

from pydantic import BaseModel


class Product(BaseModel):
    id: int
    name: str
    category_name: str
    brand_name: str
    price: Decimal
