from fastapi import APIRouter, Depends, Request
from starlette.templating import Jinja2Templates

from storages.application.state import DatabaseConnection, get_templates

from .services import get_product, list_products

router = APIRouter(prefix="/products")


@router.get("/")
async def list_products_route(
    request: Request,
    conn: DatabaseConnection,
    limit: int = 10,
    offset: int = 0,
    templates: Jinja2Templates = Depends(get_templates),
):
    products = await list_products(
        conn=conn,
        limit=limit + 1,
        offset=offset,
    )

    return templates.TemplateResponse(
        name="products.html",
        context={
            "request": request,
            "products": products[:limit],
            "limit": limit,
            "offset": offset,
            "has_next": len(products) > limit,
            "has_prev": offset > 0,
        },
    )
