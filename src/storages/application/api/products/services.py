from pydantic import TypeAdapter
from sqlalchemy import Text, cast, false, func, null, sql, true
from sqlalchemy.ext.asyncio import AsyncConnection
from sqlalchemy.orm import aliased

from storages.application.db.tables.categories import Category
from storages.application.db.tables.products import Brand
from storages.application.db.tables.products import Product as ProductTable

from .schemas import Product


async def get_product_names(conn: AsyncConnection) -> list[str]:
    query = sql.select(ProductTable.name).order_by(ProductTable.name)
    result = await conn.execute(query)
    return list(result.scalars().all())


async def list_products(
    conn: AsyncConnection,
    limit: int = 10,
    offset: int = 0,
) -> list[Product]:
    cte = (
        sql.select(
            Category.id,
            Category.parent_id,
            Category.name,
            cast(Category.name, Text).label("full_name"),
            null().label("parent_full_name"),
            false().label("is_bottom"),
            true().label("is_top"),
        )
        .where(Category.parent_id.is_(None))
        .cte(name="cte", recursive=True)
    )

    c, c3 = aliased(Category), aliased(Category)

    child_query = sql.select(
        c.id,
        c.parent_id,
        c.name,
        cast(func.concat(cte.c.full_name, ".", c.name), Text),
        cte.c.full_name.label("parent_full_name"),
        (~sql.exists(c3).where(c3.parent_id == c.id).label("is_bottom")),
        c.parent_id.is_(None),
    ).join(cte, cte.c.id == c.parent_id)

    categories_cte = cte.union_all(child_query)
    query = (
        sql.select(
            ProductTable.id,
            ProductTable.name,
            categories_cte.c.full_name.label("category_name"),
            Brand.name.label("brand_name"),
            ProductTable.price,
        )
        .join(categories_cte, categories_cte.c.id == ProductTable.category_id)
        .join(Brand, Brand.id == ProductTable.brand_id)
        .limit(limit)
        .offset(offset)
        .order_by(ProductTable.created_at.desc())
    )

    result = await conn.execute(query)
    return TypeAdapter(list[Product]).validate_python(result.mappings().all())


async def get_product():
    pass
