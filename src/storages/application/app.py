import os

import uvicorn
from fastapi import Depends, FastAPI, Request, Response
from fastapi.staticfiles import StaticFiles
from starlette import status
from starlette.middleware.base import RequestResponseEndpoint
from starlette.templating import Jinja2Templates

from .api.campaigns.views import router as campaigns_router
from .api.database.views import router as database_router
from .api.export.views import router as export_router
from .api.orders.views import router as orders_router
from .api.products.views import router as products_router
from .api.reports.views import router as reports_router
from .state import db_engine, get_templates

app = FastAPI()
app.include_router(campaigns_router)
app.include_router(export_router)
app.include_router(orders_router)
app.include_router(products_router)
app.include_router(reports_router)
app.include_router(database_router)
app.mount(
    "/static",
    StaticFiles(directory=os.path.join(os.path.dirname(__file__), "static")),
    name="static",
)
templates = Jinja2Templates(
    directory=os.path.join(os.path.dirname(__file__), "templates")
)


@app.middleware("http")
async def request_state_middleware(
    request: Request,
    call_next: RequestResponseEndpoint,
) -> Response:
    async with db_engine.connect() as conn:
        request.state.templates = templates
        request.state.dbconn = conn
        return await call_next(request)


@app.get("/")
async def hello(
    request: Request,
    templates: Jinja2Templates = Depends(get_templates),
):
    return templates.TemplateResponse(
        name="index.html",
        context={"request": request},
    )


@app.exception_handler(status.HTTP_404_NOT_FOUND)
async def not_found(request: Request, _):
    return templates.TemplateResponse(
        name="404.html",
        context={"request": request},
    )


if __name__ == "__main__":
    uvicorn.run("storages.application.app:app", reload=True)
