class NotFoundException(Exception):
    pass


class ClientError(Exception):
    pass
