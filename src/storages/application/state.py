from typing import Annotated

from fastapi import Depends, Request
from sqlalchemy import URL, make_url
from sqlalchemy.ext.asyncio import (
    AsyncConnection,
    AsyncEngine,
    create_async_engine,
)
from starlette.templating import Jinja2Templates

from storages.settings import settings


def get_templates(request: Request) -> Jinja2Templates:
    return request.state.templates


def get_async_engine(url: str | URL) -> AsyncEngine:
    return create_async_engine(
        make_url(url).set(drivername="postgresql+psycopg_async"),
        echo=settings.db_echo,
        pool_size=20,
        max_overflow=10,
        connect_args={"application_name": "FastAPI-Backend"},
    )


def get_db_conn(request: Request) -> AsyncConnection:
    return request.state.dbconn


DatabaseConnection = Annotated[AsyncConnection, Depends(get_db_conn)]
db_engine = get_async_engine(settings.db_url)
