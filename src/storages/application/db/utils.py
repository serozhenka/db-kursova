from typing import Any

from sqlalchemy import Function, func


def json_build_object(values: dict[Any, Any]) -> Function[Any]:
    args = tuple(x for item in values.items() for x in item)
    return func.json_build_object(*args)
