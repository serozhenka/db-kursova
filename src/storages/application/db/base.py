from typing import Annotated

from sqlalchemy import BigInteger, Integer, String
from sqlalchemy.orm import DeclarativeBase, mapped_column

intpk = Annotated[int, mapped_column(Integer, primary_key=True)]
strpk = Annotated[str, mapped_column(String, primary_key=True)]
bigintpk = Annotated[int, mapped_column(BigInteger, primary_key=True)]


class BasePublic(DeclarativeBase):
    __allow_unmapped__ = True
