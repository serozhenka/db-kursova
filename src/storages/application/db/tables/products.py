from datetime import datetime
from decimal import Decimal

from sqlalchemy.orm import Mapped

from ..base import BasePublic, bigintpk, intpk


class Product(BasePublic):
    __tablename__ = "products"

    id: Mapped[bigintpk]
    name: Mapped[str]
    description: Mapped[str | None]
    image_url: Mapped[str | None]
    category_id: Mapped[int]
    brand_id: Mapped[int]
    price: Mapped[Decimal]
    updated_at: Mapped[datetime]
    created_at: Mapped[datetime]


class Brand(BasePublic):
    __tablename__ = "brands"

    id: Mapped[intpk]
    name: Mapped[str]
    image_url: Mapped[str]
    updated_at: Mapped[datetime]
    created_at: Mapped[datetime]
