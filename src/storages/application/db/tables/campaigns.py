from datetime import datetime

from sqlalchemy import ForeignKey
from sqlalchemy.orm import Mapped, mapped_column

from ..base import BasePublic, bigintpk, intpk


class DeliveryChannel(BasePublic):
    __tablename__ = "delivery_channels"

    id: Mapped[intpk]
    name: Mapped[str]


class Topic(BasePublic):
    __tablename__ = "topics"

    id: Mapped[intpk]
    name: Mapped[str]


class MessageType(BasePublic):
    __tablename__ = "message_types"

    id: Mapped[intpk]
    name: Mapped[str]


class Campaign(BasePublic):
    __tablename__ = "campaigns"

    id: Mapped[bigintpk]
    name: Mapped[str]
    description: Mapped[str | None]
    type_id: Mapped[int] = mapped_column(
        ForeignKey(MessageType.id, ondelete="CASCADE")
    )
    channel_id: Mapped[int] = mapped_column(
        ForeignKey(DeliveryChannel.id, ondelete="CASCADE")
    )
    topic_id: Mapped[int] = mapped_column(
        ForeignKey(Topic.id, ondelete="CASCADE")
    )
    is_ab_test: Mapped[bool]
    hour_sending_limit: Mapped[int]
    subject_length: Mapped[int]
    subject_with_personalization: Mapped[bool]
    subject_with_deadline: Mapped[bool]
    subject_with_emoji: Mapped[bool]
    subject_with_bonuses: Mapped[bool]
    subject_with_discount: Mapped[bool]
    subject_with_saleout: Mapped[bool]
    priority: Mapped[int]
    started_at: Mapped[datetime]
    finished_at: Mapped[datetime | None]
    updated_at: Mapped[datetime]
