from datetime import date, datetime
from enum import StrEnum

from sqlalchemy.orm import Mapped

from ..base import BasePublic, bigintpk


class Gender(StrEnum):
    MALE = "MALE"
    FEMALE = "FEMALE"


class Customer(BasePublic):
    __tablename__ = "customers"

    id: Mapped[bigintpk]
    first_name: Mapped[str]
    last_name: Mapped[str]
    email: Mapped[str]
    phone_number: Mapped[str | None]
    birthdate: Mapped[date]
    gender: Mapped[Gender]
    country_code: Mapped[str]
    joined_at: Mapped[datetime]
    updated_at: Mapped[datetime]
