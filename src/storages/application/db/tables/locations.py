from datetime import datetime

from sqlalchemy.orm import Mapped

from ..base import BasePublic, intpk, strpk


class Address(BasePublic):
    __tablename__ = "addresses"

    id: Mapped[intpk]
    country: Mapped[str]
    state: Mapped[str | None]
    city: Mapped[str]
    street: Mapped[str]
    suite: Mapped[str | None]
    appartment: Mapped[str]
    created_at: Mapped[datetime]


class Country(BasePublic):
    __tablename__ = "countries"

    alpha2: Mapped[strpk]
    name: Mapped[str]
    updated_at: Mapped[datetime]
    created_at: Mapped[datetime]
