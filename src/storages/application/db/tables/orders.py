from datetime import datetime
from decimal import Decimal
from enum import StrEnum

from sqlalchemy import BigInteger, ForeignKey, PrimaryKeyConstraint
from sqlalchemy.orm import Mapped, mapped_column

from ..base import BasePublic, bigintpk
from .customers import Customer
from .locations import Address


class OrderStatus(StrEnum):
    CREATED = "CREATED"
    PAYED = "PAYED"
    READY_FOR_DELIVERY = "READY_FOR_DELIVERY"
    DELIVERING = "DELIVERING"
    DELIVERED = "DELIVERED"
    CLOSED = "CLOSED"


class Order(BasePublic):
    __tablename__ = "orders"

    id: Mapped[bigintpk]
    status: Mapped[OrderStatus]
    customer_id: Mapped[int] = mapped_column(
        ForeignKey(Customer.id, ondelete="CASCADE")
    )
    address_id: Mapped[int] = mapped_column(
        ForeignKey(Address.id, ondelete="CASCADE")
    )
    created_at: Mapped[datetime]
    updated_at: Mapped[datetime]


class OrderLine(BasePublic):
    __tablename__ = "order_lines"

    order_id: Mapped[int] = mapped_column(BigInteger)
    product_id: Mapped[int] = mapped_column(BigInteger)
    price: Mapped[Decimal]
    quantity: Mapped[int]
    created_at: Mapped[datetime]
    updated_at: Mapped[datetime]

    __table_args__ = (
        PrimaryKeyConstraint(
            "order_id",
            "product_id",
            name="order_lines_order_id_product_id_pk",
        ),
    )
