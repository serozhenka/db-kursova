from datetime import datetime

from sqlalchemy import ForeignKey
from sqlalchemy.orm import Mapped, mapped_column

from ..base import BasePublic, BigInteger, intpk
from .campaigns import Campaign
from .customers import Customer
from .orders import Order


class Platform(BasePublic):
    __tablename__ = "platforms"

    id: Mapped[intpk]
    name: Mapped[str]


class Message(BasePublic):
    __tablename__ = "messages"

    id: Mapped[intpk]
    campaign_id: Mapped[int] = mapped_column(
        BigInteger,
        ForeignKey(Campaign.id, ondelete="CASCADE"),
    )
    customer_id: Mapped[int] = mapped_column(
        BigInteger,
        ForeignKey(Customer.id, ondelete="CASCADE"),
    )
    platform_id: Mapped[int] = mapped_column(
        ForeignKey(Platform.id, ondelete="CASCADE"),
    )
    opened_at: Mapped[datetime | None]
    clicked_at: Mapped[datetime | None]
    unsubscribed_at: Mapped[datetime | None]
    purchased_at: Mapped[datetime | None]
    order_id: Mapped[int | None] = mapped_column(
        BigInteger,
        ForeignKey(Order.id, ondelete="CASCADE"),
    )
    sent_at: Mapped[datetime]
    created_at: Mapped[datetime]
    updated_at: Mapped[datetime]
