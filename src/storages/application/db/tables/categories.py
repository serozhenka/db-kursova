from datetime import datetime

from sqlalchemy import ForeignKey
from sqlalchemy.orm import Mapped, mapped_column

from ..base import BasePublic, intpk


class Category(BasePublic):
    __tablename__ = "categories"

    id: Mapped[intpk]
    name: Mapped[str]
    parent_id: Mapped[int] = mapped_column(
        ForeignKey("categories.id", ondelete="CASCADE")
    )
    updated_at: Mapped[datetime]
    created_at: Mapped[datetime]
