from typing import get_type_hints


class ModelMeta(type):
    def __new__(cls, name, bases, dct):
        print(name, bases, dct)
        x = super().__new__(cls, name, bases, dct)
        return x


class Model:
    @classmethod
    def create_table(cls):
        attrs = get_type_hints(cls)
        for attr, tp in attrs.items():
            print(attr, tp, getattr(cls, attr))
