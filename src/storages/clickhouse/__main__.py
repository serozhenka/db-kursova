import argparse

from ..utils import measure_time
from .delete import delete
from .init import init

if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument("--init", action="store_true")
    parser.add_argument("--delete", action="store_true")
    args, unknown = parser.parse_known_args()

    if args.init:
        with measure_time("Init process"):
            init()
    elif args.delete:
        with measure_time("Delete process"):
            delete()
