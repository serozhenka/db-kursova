from clickhouse_connect import get_client
from clickhouse_connect.driver.client import Client


def clickhouse_client() -> Client:
    return get_client(host="localhost", port=8123)
