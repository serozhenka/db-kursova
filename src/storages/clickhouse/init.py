from .utils import clickhouse_client


def init():
    print("Started init process")
    client = clickhouse_client()

    client.command(
        """
        CREATE TABLE FactMessages (
            CampaignKey UInt32 NOT NULL,
            ProductKey String(32) NOT NULL,
            SubjectKey UInt32 NOT NULL,
            PlatformName String(16) NOT NULL,
            DateKey UInt32 NOT NULL,
            TimeKey UInt32 NOT NULL,
            ClickedTimes UInt32 NOT NULL,
            OpenedTimes UInt32 NOT NULL,
            SentTimes UInt32 NOT NULL,
            UnsubscribedTimes UInt32 NOT NULL,
            PurchasedTimes UInt32 NOT NULL,
            TotalPrice Decimal64(2) NOT NULL
        )
        PRIMARY KEY (CampaignKey, ProductKey, PlatformName, DateKey, TimeKey)
        """
    )

    client.command(
        """
        ALTER TABLE FactMessages
        ADD INDEX FactMessages_ProductKeyDateKey_Index (ProductKey, DateKey)
        TYPE bloom_filter
        """
    )

    client.command(
        """
        ALTER TABLE FactMessages
        ADD INDEX FactMessages_ProductKeyTimeKey_Index (ProductKey, TimeKey)
        TYPE bloom_filter
        """
    )

    client.command(
        """
        CREATE TABLE DimSubjects (
            Key UInt32 NOT NULL,
            OltpKey UInt64 NOT NULL,
            Length UInt16 NOT NULL,
            WithPersonalization Bool NOT NULL,
            WithDeadline Bool NOT NULL,
            WithEmoji Bool NOT NULL,
            WithBonuses Bool NOT NULL,
            WithDiscount Bool NOT NULL,
            WithSaleout Bool NOT NULL
        )
        PRIMARY KEY Key
        """
    )

    client.command(
        """
        CREATE TABLE DimCampaigns (
            Key UInt32 NOT NULL,
            OltpKey UInt64 NOT NULL,
            Name String(64) NOT NULL,
            TypeName String(16) NOT NULL,
            ChannelName String(16) NOT NULL,
            TopicName String(32) NOT NULL,
            DateKey UInt32 NOT NULL,
            IsAbTest Bool NOT NULL,
            IsActive Bool NOT NULL DEFAULT true
        )
        PRIMARY KEY Key
        """
    )

    client.command(
        """
        CREATE TABLE DimTime (
            TimeKey UInt32 NOT NULL,
            Minute UInt8 NOT NULL,
            Hour UInt8 NOT NULL
        )
        PRIMARY KEY TimeKey
        """
    )

    client.command(
        """
        CREATE TABLE DimDate (
            DateKey UInt32 NOT NULL,
            Date Date NOT NULL,
            DayOfTheWeek String(16) NOT NULL,
            Month String(64) NOT NULL,
            Year UInt16 NOT NULL
        )
        PRIMARY KEY DateKey
        """
    )

    client.command(
        """
        CREATE TABLE DimProducts (
            Key String(32) NOT NULL,
            OltpKey UInt64 NOT NULL,
            Name String(64) NOT NULL,
            BrandName String(64) NOT NULL,
            CategoryKey String(32) NOT NULL,
            Price Decimal64(2) NOT NULL,
            IsActive Bool NOT NULL DEFAULT true
        )
        PRIMARY KEY Key
        """
    )

    client.command(
        """
        ALTER TABLE DimProducts
        ADD INDEX DimProducts_OltpKeyIsActive_Index (OltpKey, IsActive)
        TYPE bloom_filter
        """
    )

    client.command(
        """
        CREATE TABLE DimCategories (
            Key String(32) NOT NULL,
            OltpKey UInt64 NOT NULL,
            ParentKey String(32) NOT NULL,
            Name String(64) NOT NULL,
            ParentName String(64) NOT NULL,
            Distance UInt8 NOT NULL,
            IsBottomFlag Bool NOT NULL,
            IsTopFlag Bool NOT NULL,
            IsActive Bool NOT NULL DEFAULT true,
        )
        PRIMARY KEY Key
        """
    )

    client.command(
        """
        ALTER TABLE DimCategories
        ADD INDEX DimCategories_OltpKeyIsActive_Index (OltpKey, IsActive)
        TYPE bloom_filter
        """
    )

    client.command(
        """
        CREATE TABLE FactSales (
            SaleOltpKey UInt64 NOT NULL,
            ProductKey String(32) NOT NULL,
            CustomerKey UInt32 NOT NULL,
            LocationKey UInt32 NOT NULL,
            DateKey UInt32 NOT NULL,
            TimeKey UInt32 NOT NULL,
            Quantity UInt32 NOT NULL,
            Price Decimal64(2) NOT NULL
        )
        PRIMARY KEY (ProductKey, CustomerKey, LocationKey, DateKey, TimeKey)
        """
    )

    client.command(
        """
        ALTER TABLE FactSales
        ADD INDEX FactSales_SaleOltpKey_Index (SaleOltpKey)
        TYPE bloom_filter
        """
    )

    client.command(
        """
        CREATE TABLE DimLocations (
            Key UInt32 NOT NULL,
            CityName String(64) NOT NULL,
            StateName String(64) NOT NULL,
            CountryAlpha2 String(2) NOT NULL,
            CountryName String(64) NOT NULL
        )
        PRIMARY KEY Key
        """
    )

    client.command(
        """
        ALTER TABLE DimLocations
        ADD INDEX FactMessages_CityNameStateNameCountryName_Index (CityName, StateName, CountryName)
        TYPE bloom_filter
        """
    )

    client.command(
        """
        CREATE TABLE DimCustomers (
            Key UInt32 NOT NULL,
            OltpKey UInt64 NOT NULL,
            Birthdate Datetime NOT NULL,
            AgeGroupKey UInt32 NOT NULL,
            Gender String(1) NOT NULL,
            LocationKey UInt32 NOT NULL,
            IsActive Bool NOT NULL
        )
        PRIMARY KEY Key
        """
    )

    client.command(
        """
        ALTER TABLE DimCustomers
        ADD INDEX DimCustomers_OltpKeyIsActive_Index (OltpKey, IsActive)
        TYPE bloom_filter
        """
    )

    client.command(
        """
        CREATE TABLE DimAgeGroups (
            Key UInt32 NOT NULL,
            Name String(32) NOT NULL,
            StartAge UInt8 NOT NULL,
            EndAge UInt8 NOT NULL
        )
        PRIMARY KEY Key
        """
    )
