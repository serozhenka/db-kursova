from .utils import clickhouse_client
from ..etl.utils import delete_config


def delete():
    print("Started delete process")
    client = clickhouse_client()

    client.command("DROP TABLE DimAgeGroups")
    client.command("DROP TABLE DimCustomers")
    client.command("DROP TABLE DimLocations")
    client.command("DROP TABLE FactSales")
    client.command("DROP TABLE DimCategories")
    client.command("DROP TABLE DimProducts")
    client.command("DROP TABLE DimTime")
    client.command("DROP TABLE DimDate")
    client.command("DROP TABLE DimCampaigns")
    client.command("DROP TABLE DimSubjects")
    client.command("DROP TABLE FactMessages")

    delete_config()
